<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\WorkCategory;
use Faker\Generator as Faker;

$factory->define(WorkCategory::class, function (Faker $faker) {
    $title = $faker->words(rand(1, 3), true);

    return [
        'title' => $title,
        'body' => '<p>' . implode('</p><p>', $faker->paragraphs(rand(1, 3))) . '</p>',
        'slug' => strtolower(str_slug($title)),
    ];
});
