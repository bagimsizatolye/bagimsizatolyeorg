<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Blog;
use Faker\Generator as Faker;

$factory->define(Blog::class, function (Faker $faker) {
    $title = $faker->words(rand(2, 7), true);

    return [
        'title' => ucwords($title),
        'slug' => str_slug($title),
        'body' => '<p>' . implode('</p><p>', $faker->paragraphs(rand(1, 5))) . '</p>',
        'author_id' => function () {
            return App\Models\User::inRandomOrder()->first()->id;
        },
        'published_at' => $faker->dateTimeInInterval($startDate = '-1 years', $interval = '+1 years'),
        'featured_at' => rand(0, 3) === 0 ? $faker->dateTimeInInterval($startDate = '-1 years', $interval = '+1 years') : null,
    ];
});
