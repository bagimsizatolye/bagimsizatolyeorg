<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Tag;
use Faker\Generator as Faker;

$factory->define(Tag::class, function (Faker $faker) {
    $title = $faker->words(rand(1, 3), true);

    return [
        'title' => ucwords($title),
        'slug' => str_slug($title),
    ];
});
