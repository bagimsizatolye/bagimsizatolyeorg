<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\ContactForm;
use Faker\Generator as Faker;

$factory->define(ContactForm::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'subject' => $faker->sentence(rand(1, 9)),
        'text' => $faker->paragraphs(rand(1, 10), true),
    ];
});
