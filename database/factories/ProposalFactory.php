<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Proposal;
use Faker\Generator as Faker;

$factory->define(Proposal::class, function (Faker $faker) {
    $acceptedAt = rand(1, 4) === 1 ? $faker->dateTimeBetween('-1 year', 'now') : null;
    $rejectedAt = null;

    if (is_null($acceptedAt)) {
        $rejectedAt = rand(1, 4) === 1 ? $faker->dateTimeBetween('-1 year', 'now') : null;
    }

    $types = array_keys(config('ba.proposal_types'));

    return [
        'concern_company_name' => mb_convert_case($faker->company, MB_CASE_TITLE),
        'concern_person_name' => $faker->name,
        'accepted_at' => $acceptedAt,
        'rejected_at' => $rejectedAt,
        'proposal_type' => array_random($types),
        'proposed_on' => $faker->dateTimeInInterval($startDate = '-1 years', $interval = '+1 years'),
        'expires_on' => $faker->dateTimeInInterval($startDate = '+1 years', $interval = '+1 years'),
        'code' => strtolower(str_random(8)),
        'delivery_days' => rand(5, 30),
        'bank_account' => $faker->iban('tr'),
    ];
});
