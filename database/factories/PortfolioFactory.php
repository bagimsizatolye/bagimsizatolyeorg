<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Portfolio;
use Faker\Generator as Faker;

$factory->define(Portfolio::class, function (Faker $faker) {
    $title = $faker->words(rand(1, 5), true);

    return [
        'title' => ucwords($title),
        'slug' => strtolower(str_slug($title)),
        'body' => '<p>' . implode('</p><p>', $faker->paragraphs(rand(1, 5))) . '</p>',
        'published_at' => $faker->dateTimeInInterval($startDate = '-1 years', $interval = '+1 years'),
        'view_type_id' => $faker->randomElement(['image', 'text']),
        'description' => $faker->words(rand(2, 4), true),
        'customer_name' => $faker->words(rand(2, 4), true),
        'featured_at' => rand(0, 3) === 0 ? $faker->dateTimeInInterval($startDate = '-1 years', $interval = '+1 years') : null,
    ];
});
