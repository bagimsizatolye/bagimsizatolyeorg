<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('related_id')->unsigned()->nullable();
            $table->string('related_to', 100);
            $table->string('path', 1000);
            $table->string('path_thumbnail', 1000)->nullable();
            $table->string('type_id', 100)->default('image');
            $table->integer('order_num')->default(0)->index();
            $table->string('title', 3000)->nullable();
            $table->integer('width')->default(0);
            $table->integer('height')->default(0);
            $table->boolean('is_featured')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
