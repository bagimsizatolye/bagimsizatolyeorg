<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('concern_company_name', 300)->nullable();
            $table->string('concern_person_name', 300)->nullable();
            $table->string('proposal_type', 100);
            $table->date('proposed_on');
            $table->date('expires_on')->nullable();
            $table->char('code', 8)->unique();
            $table->mediumInteger('delivery_days')->nullable();
            $table->string('bank_account', 100)->nullable();
            $table->timestamp('accepted_at')->nullable();
            $table->timestamp('rejected_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
