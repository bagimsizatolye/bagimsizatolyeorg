<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_relations', function (Blueprint $table) {
            $table->integer('portfolio_id')->unsigned();
            $table->integer('related_portfolio_id')->unsigned();
            $table->integer('order_num')->default(1);
            $table->primary([
                'portfolio_id',
                'related_portfolio_id',
            ]);

            $table->foreign('portfolio_id')
                ->references('id')->on('portfolios')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('related_portfolio_id')
                ->references('id')->on('portfolios')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_relations');
    }
}
