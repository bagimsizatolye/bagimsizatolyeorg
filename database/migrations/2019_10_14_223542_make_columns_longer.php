<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeColumnsLonger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->json('title')->change();
        });

        Schema::table('work_categories', function (Blueprint $table) {
            $table->json('title')->change();
            $table->json('body')->change();
            $table->json('slug')->change();
        });

        Schema::table('portfolios', function (Blueprint $table) {
            $table->json('title')->change();
            $table->json('body')->change();
            $table->json('slug')->change();
            $table->json('description')->change();
            $table->json('customer_name')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('title', 255)->change();
        });

        Schema::table('work_categories', function (Blueprint $table) {
            $table->string('title', 100)->change();
            $table->longText('body')->change();
            $table->string('slug', 250)->change();
        });

        Schema::table('portfolios', function (Blueprint $table) {
            $table->string('title', 150)->change();
            $table->longText('body')->change();
            $table->string('slug', 250)->change();
            $table->string('description', 150)->change();
            $table->string('customer_name', 150)->change();
        });
    }
}
