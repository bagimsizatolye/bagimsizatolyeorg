<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Portfolio;
use App\Models\WorkCategory;
use App\Models\User;

class MakeColumnsTranslatable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $list = DB::table('work_categories')->get();

        foreach ($list as $row) {
            $item = WorkCategory::find($row->id);

            if ($item->title === '') {
                $item->setTranslation('title', 'tr', $row->title);
            }

            if ($item->body === '') {
                $item->setTranslation('body', 'tr', $row->body);
            }

            if ($item->slug === '') {
                $item->setTranslation('slug', 'tr', $row->slug);
            }

            $item->save();
        }

        $list = DB::table('portfolios')->whereNull('deleted_at')->get();

        foreach ($list as $row) {
            $item = Portfolio::find($row->id);


            if ($item->title === '') {
                $item->setTranslation('title', 'tr', $row->title);
            }

            if ($item->body === '') {
                $item->setTranslation('body', 'tr', $row->body);
            }

            if ($item->slug === '') {
                $item->setTranslation('slug', 'tr', $row->slug);
            }

            if ($item->description === '') {
                $item->setTranslation('description', 'tr', $row->description);
            }

            if ($item->customer_name === '') {
                $item->setTranslation('customer_name', 'tr', $row->customer_name);
            }

            $item->save();
        }

        $list = DB::table('users')->get();

        foreach ($list as $row) {
            $item = User::find($row->id);

            if ($item->title === '') {
                $item->setTranslation('title', 'tr', $row->title);
            }

            if ($item->description === '' and $item->description !== null) {
                $item->setTranslation('description', 'tr', $row->description);
            }

            $item->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
