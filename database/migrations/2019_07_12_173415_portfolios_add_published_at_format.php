<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PortfoliosAddPublishedAtFormat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portfolios', function (Blueprint $table) {
            $table->string('published_at_format', 20)->default('MMMM YYYY');
            $table->dropColumn('published_at');
        });

        Schema::table('portfolios', function (Blueprint $table) {
            $table->date('published_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portfolios', function (Blueprint $table) {
            $table->dropColumn('published_at_format');
            $table->dropColumn('published_at');
        });

        Schema::table('portfolios', function (Blueprint $table) {
            $table->timestamp('published_at')->nullable();
        });
    }
}
