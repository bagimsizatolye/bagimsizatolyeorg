<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 150);
            $table->string('slug', 250)->unique();
            $table->longText('body');
            $table->timestamp('published_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('porfolio_users', function (Blueprint $table) {
            $table->integer('portfolio_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('title', 500)->nullable();
            $table->primary(['portfolio_id', 'user_id']);

            $table->foreign('portfolio_id')
                ->references('id')->on('portfolios')
                ->onUpdate('restrict')->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('restrict')->onDelete('restrict');
        });

        Schema::create('porfolio_categories', function (Blueprint $table) {
            $table->integer('portfolio_id')->unsigned();
            $table->integer('work_category_id')->unsigned();
            $table->primary(['portfolio_id', 'work_category_id']);

            $table->foreign('portfolio_id')
                ->references('id')->on('portfolios')
                ->onUpdate('restrict')->onDelete('cascade');

            $table->foreign('work_category_id')
                ->references('id')->on('work_categories')
                ->onUpdate('restrict')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('porfolio_users');
        Schema::dropIfExists('porfolio_categories');
        Schema::dropIfExists('portfolios');
    }
}
