<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 250);
            $table->string('slug', 300)->unique();
            $table->longText('body');
            $table->integer('author_id')->unsigned();
            $table->timestamp('published_at')->nullable();
            $table->timestamp('featured_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('author_id')
                ->references('id')->on('users')
                ->onUpdate('restrict')->onDelete('restrict');
        });

        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 250);
            $table->string('slug', 300)->unique();
        });

        Schema::create('blog_tags', function (Blueprint $table) {
            $table->integer('blog_id')->unsigned();
            $table->integer('tag_id')->unsigned();

            $table->foreign('blog_id')
                ->references('id')->on('blogs')
                ->onUpdate('restrict')->onDelete('cascade');

            $table->foreign('tag_id')
                ->references('id')->on('tags')
                ->onUpdate('restrict')->onDelete('cascade');

            $table->primary([
                'blog_id',
                'tag_id',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_tags');
        Schema::dropIfExists('tags');
        Schema::dropIfExists('blogs');
    }
}
