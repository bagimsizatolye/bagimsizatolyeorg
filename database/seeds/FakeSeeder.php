<?php

use Illuminate\Database\Seeder;

class FakeSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /**
         * @var Illuminate\Database\Eloquent\Collection;
         */
        // $categories = factory(App\Models\WorkCategory::class, rand(4, 8))
        //     ->create()
        //     ->each(function (App\Models\WorkCategory $category) {
        //         $media = factory(App\Models\Media::class, 1)
        //             ->states('work-category-icon')
        //             ->create([
        //                 'related_to' => 'work-category-icon',
        //                 'related_id' => $category->id,
        //             ]);
        //     });
        //
        // $users = factory(App\Models\User::class, rand(3, 7))
        //     ->create();

        // $categories = App\Models\WorkCategory::all();
        // $users = App\Models\User::all();
        //
        // $portfolios = factory(App\Models\Portfolio::class, rand(5, 10))
        //     ->create()
        //     ->each(function (App\Models\Portfolio $portfolio) use ($categories, $users) {
        //         $portfolio->workCategories()->attach($categories->random(rand(1, 4)));
        //         $portfolio->users()->attach($users->random(rand(1, 4)));
        //         $media = factory(App\Models\Media::class, rand(3, 5))
        //             ->states('portfolio')
        //             ->create([
        //                 'related_to' => 'portfolio',
        //                 'related_id' => $portfolio->id,
        //             ]);
        //     });

        // $contactForms = factory(App\Models\ContactForm::class, rand(10, 20))
        //     ->create();

        // $tags = factory(App\Models\Tag::class, rand(10, 20))
        //     ->create();
        //
        // $blogs = factory(App\Models\Blog::class, rand(10, 20))
        //     ->create()
        //     ->each(function (App\Models\Blog $blog) {
        //         factory(App\Models\Media::class)->create([
        //             'related_to' => 'blog-cover',
        //             'related_id' => $blog->id,
        //         ]);
        //     });

        $proposals = factory(App\Models\Proposal::class, 30)
            ->create();
    }
}
