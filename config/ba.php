<?php

return [
    /**
     * Used for notifying some events in the website.
     */
    'admin_email' => env('ADMIN_EMAIL'),

    'contact_phone' => env('CONTACT_PHONE'),
    'contact_telegram' => env('CONTACT_TELEGRAM'),

    /**
     * Used for App\Models\Text
     */
    'available_languages' => [
        'tr',
        'en',
    ],

    'texts' => [
        'home_title',
        'home_title_sub',
        'home_box_1_title',
        'home_box_1_text',
        'home_box_2_title',
        'home_box_2_text',
        'home_box_3_title',
        'home_box_3_text',
        'home_box_4_title',
        'home_box_4_text',
        'home_banner_title',
        'home_banner_text',
        'home_works_title',
        'home_works_text',
        'manifest_title',
        'manifest_title_sub',
        'services_title',
        'services_title_sub',
        'services_text_title',
        'services_text',
        'portfolio_title',
        'portfolio_title_sub',
        'blog_title',
        'blog_title_sub',
        'contact_title',
        'contact_title_sub',
        'contact_text',
        'proposal_intro',
        'seo_title',
        'seo_description',
        'seo_keywords',
        'seo_manifest_title',
        'seo_manifest_description',
        'seo_manifest_keywords',
        'seo_services_title',
        'seo_services_description',
        'seo_services_keywords',
        'seo_portfolio_title',
        'seo_portfolio_description',
        'seo_portfolio_keywords',
        'seo_blog_title',
        'seo_blog_description',
        'seo_blog_keywords',
        'seo_contact_title',
        'seo_contact_description',
        'seo_contact_keywords',
    ],

    'portfolio_types' => [
        'image',
        'text',
        'video',
    ],

    'proposal_types' => [
        'proposal.2020-generic' => '2020 Genel',
        'proposal.2020-yaykoop' => '2020 Yaykoop',
    ],

    'prices' => [
        'basic_2020' => env('BA_PRICE_BASIC_2020'),
        'basic_yaykoop_2020' => env('BA_PRICE_BASIC_YAYKOOP_2020'),

        'mailchimp' => env('BA_PRICE_MAILCHIMP'),
        'domain' => env('BA_PRICE_DOMAIN'),
        'multilanguage' => env('BA_PRICE_MULTILANGUAGE'),
        'content_writing' => env('BA_PRICE_CONTENT_WRITING'),
        'logo_design' => env('BA_PRICE_LOGO_DESIGN'),
        'maintenance' => env('BA_PRICE_MAINTENANCE'),
        'email_setup' => env('BA_PRICE_EMAIL_SETUP'),
        'ecommerce' => env('BA_PRICE_ECOMMERCE'),
    ],
];
