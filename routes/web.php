<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['locale:tr'])->group(function () {
    Auth::routes(['register' => false]);

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/manifesto', 'HomeController@manifest')->name('manifest');
    Route::get('/maharetlerimiz', 'HomeController@services')->name('services');
    Route::get('/islerimiz', 'HomeController@portfolio')->name('portfolio.index');
    Route::get('/islerimiz/{slug}', 'HomeController@portfolioShow')
        ->name('portfolio.show');
    Route::any('/iletisim', 'HomeController@contact')->name('contact');
    Route::get('/seyir-defteri', 'HomeController@blogIndex')->name('blog.index');
    Route::get('/seyir-defteri/{slug}', 'HomeController@blogShow')->name('blog.show');
    Route::get('/seyir-defteri/etiket/{slug}', 'HomeController@tagShow')
        ->name('blog.tag.show');
    Route::get('/fiyat-listesi/{proposalCode}', 'ProposalController@show')
        ->name('proposal.show');
});

Route::middleware(['locale:en'])->prefix('en')->name('en.')->group(function () {
    Auth::routes(['register' => false]);

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/manifest', 'HomeController@manifest')->name('manifest');
    Route::get('/craft', 'HomeController@services')->name('services');
    Route::get('/works', 'HomeController@portfolio')->name('portfolio.index');
    Route::get('/works/{slug}', 'HomeController@portfolioShow')
        ->name('portfolio.show');
    Route::any('/contact', 'HomeController@contact')->name('contact');
    Route::get('/journal', 'HomeController@blogIndex')->name('blog.index');
    Route::get('/journal/{slug}', 'HomeController@blogShow')->name('blog.show');
    Route::get('/journal/tag/{slug}', 'HomeController@tagShow')
        ->name('blog.tag.show');
    Route::get('/price-list/{proposalCode}', 'ProposalController@show')
        ->name('proposal.show');
});

Route::middleware(['auth'])->namespace('Admin')->prefix('admin')->name('admin.')->group(function () {
    Route::get('/', 'AdminController@index')->name('home');
    Route::resource('contactform', 'ContactFormController');
    Route::resource('portfolio', 'PortfolioController');
    Route::resource('blog', 'BlogController');
    Route::resource('work-category', 'WorkCategoryController');
    Route::resource('user', 'UserController');
    Route::resource('media', 'MediaController');
    Route::resource('text', 'TextController');
    Route::resource('proposal', 'ProposalController');
});
