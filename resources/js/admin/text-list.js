$(() => {
  $('.text-form [name="value"]').on('blur', function() {
    const form = $(this).parents('.text-form');
    console.log(form, this, $(this));
    const data = {
      value: form.find('[name="value"]').val(),
      key: form.find('[name="key"]').val(),
      lang: form.find('[name="lang"]').val(),
    };

    if (!data.value) {
      data.value = null;
    }

    form.find('strong').remove();

    $(this).prop('disabled', true);

    const indicator = $('<strong>');
    indicator.attr('id', 'ind-' + data.key + data.lang);
    indicator.text('Kaydediliyor');
    form.append(indicator);

    $.ajax({
      method: 'post',
      url: form.attr('action'),
      data: data
    }).done((response) => {
      console.log(response);
      $(this).prop('disabled', false);
      indicator.text('Kaydedildi');
      setTimeout(() => {
        indicator.fadeOut();
      }, 5000);
    }).fail((response) => {
      console.error(response);
      $(this).prop('disabled', false);
      indicator.text('Hata!').addClass('text-danger');
    });

    return false;
  });
});