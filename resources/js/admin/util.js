$(function() {
  setTimeout(() => {
    $('.select2-init').each((k, v) => {
      v = $(v);
      v.select2({
        tags: v.hasClass('select2-tags'),
      });
    });
  }, 100);

  $('.summernote-init').summernote({
    minHeight: null,
    height: 500,
    tooltip: false,
    lang: 'tr-TR',
    toolbar: [
      ['style', [
        'style', 'bold', 'italic', 'underline', 'link', 'picture',
      ]],
      ['para', ['ul', 'ol', 'paragraph']],
      ['color', ['color']],
      ['misc', [
        'codeview',
      ]]
    ],
    callbacks: {
      onImageUpload: function(files) {
        const data = new FormData();
        data.append('image', files[0]);
        data.append('width', 800);
        data.append('related_to', 'blog_post');
        $('.summernote-init').summernote('disable');

        $.ajax({
          type: 'post',
          url: $(this).data('upload-url') || '/admin/media',
          contentType: false,
          processData: false,
          data: data,
        }).done(function(response) {
          console.log(response);
          const img = document.createElement('img');
          img.src = response.url;
          img.style.maxWidth = '100%';
          img.alt = response.id;
          console.log(img);
          $('.summernote-init').summernote('enable');
          return $('.summernote-init').summernote('insertNode', img);
        }).fail(function(response) {
          console.error(response);
          $('.summernote-init').summernote('enable');
        });
      },
      onPaste: function(e) {
        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
        e.preventDefault();
        setTimeout(function() {
          document.execCommand('insertText', false, bufferText);
        }, 10);
      },
    },
  });

  $('[data-ask]').on('click', function() {
    if (confirm($(this).data('ask'))) {
      return true;
    }

    return false;
  });

  $('.copy-to-clipboard').on('click', (event) => {
    event.preventDefault();

    const str = event.target.dataset.target;

    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    alert('Kopyalandı');
  });
});