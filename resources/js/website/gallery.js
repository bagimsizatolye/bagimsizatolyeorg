const PhotoSwipe = require('photoswipe');
const PhotoSwipeUI_Default = require('photoswipe/dist/photoswipe-ui-default');

document.addEventListener("DOMContentLoaded", function(event) {
  if (window.ba && window.ba.gallery) {
    document
      .querySelector(window.ba.gallery.selector)
      .onclick = function(e) {
        // console.log(e);
        // if(e.target !== this) {
        //     return;
        // }

        var pswpElement = document.querySelectorAll('.pswp')[0];
        // define options (if needed)
        var options = {
          // optionName: 'option value'
          // for example:
          index: $(this).data('start-at') || 0, // start at first slide
          shareButtons: [
            { id: 'facebook', label: 'Share on Facebook', url: 'https://www.facebook.com/sharer/sharer.php?u=@{{url}}' },
            { id: 'twitter', label: 'Tweet', url: 'https://twitter.com/intent/tweet?text=@{{text}}&url=@{{url}}' },
            { id: 'pinterest', label: 'Pin it', url: 'http://www.pinterest.com/pin/create/button/?url=@{{url}}&media=@{{image_url}}&description=@{{text}}' },
          ],
          history: false,
        };

        // Initializes and opens PhotoSwipe
        window.gallery = new PhotoSwipe(
          pswpElement,
          PhotoSwipeUI_Default,
          window.ba.gallery.items,
          options
        );
        window.gallery.init();
      };
  }
});