@extends('layouts.app')

@section('header')
  {!! text('home_title', true) !!}
  <hr clasS="my-0">
  <span class="sub-title d-block">{!! text('home_title_sub', true) !!}</span>
@endsection

@section('content')
<div class="container">
  <div class="row justify-content-around mb-5 mt-5">
    <div class="col-12 col-md-5 home-card mb-5">
      <div class="row">
        <div class="col-5 text-lg-right">
          <img src="img/page-content/home-spaceman.png" class="img-fluid">
        </div>
        <div class="col-7">
          <div class="pl-1">
            <h4><a href="{{ ba_route('services') }}">
              {{ text('home_box_1_title') }}
            </a></h4>
            <p>
              {{ text('home_box_1_text') }}
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-5 home-card mb-5">
      <div class="row">
        <div class="col-5 text-lg-right mb-5">
          <img src="img/page-content/home-tablet.png" class="img-fluid">
        </div>
        <div class="col-7">
          <div class="pl-1">
            <h4><a href="{{ ba_route('portfolio.index') }}">
              {{ text('home_box_2_title') }}
            </a></h4>
            <p>
              {{ text('home_box_2_text') }}
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-5 home-card mb-5">
      <div class="row">
        <div class="col-5 text-lg-right mb-5">
          <img src="img/page-content/home-manifest.png" class="img-fluid">
        </div>
        <div class="col-7">
          <div class="pl-1">
            <h4><a href="{{ ba_route('manifest') }}">
              {{ text('home_box_3_title') }}
            </a></h4>
            <p>
              {{ text('home_box_3_text') }}
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-5 home-card mb-5">
      <div class="row">
        <div class="col-5 text-lg-right mb-5">
          <img src="img/page-content/home-blog.png" class="img-fluid">
        </div>
        <div class="col-7">
          <div class="pl-1">
            <h4><a href="{{ ba_route('blog.index') }}">
              {{ text('home_box_4_title') }}
            </a></h4>
            <p>
              {{ text('home_box_4_text') }}
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="home-banner">
  <img src="{{ asset('img/home-banner-astronaut.png') }}" class="image">

  <div class="home-banner-content">
    <h3 class="mb-4">{{ text('home_banner_title') }}</h3>
    <p>
      {!! text('home_banner_text', true) !!}
    </p>
  </div>
</div>

<div class="home-portfolio text-center">
  <h3 class="text-bold mb-0">{{ text('home_works_title') }}</h3>
  <hr class="my-5">

  <portfolio-list-component
    portfolio-json="{!! str_replace('"', '\'', json_encode($list->toArray(), JSON_HEX_APOS | JSON_HEX_QUOT)) !!}"
    v-bind:show-categories="false"
    container-class="container"
  ></portfolio-list-component>
</div>
@endsection

@section('scripts')
  @parent

  <script>
    window.categories = {!! json_encode($workCategories->toArray(), JSON_HEX_APOS) !!}
  </script>
@endsection
