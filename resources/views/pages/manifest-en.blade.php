@extends('layouts.app')

@section('header')
  {!! text('manifest_title', true) !!}
  <hr clasS="my-0">
  <span class="sub-title d-block">{!! text('manifest_title_sub', true) !!}</span>
@endsection

@section('content')
<div class="container container-reading manifest">

  <h3>{{ __('MANİFESTO') }}</h3>

  <p>
    We spend the most productive minutes of our lives in workplaces. We need to work in order to earn our living. However, <strong>both in large corporations and in relatively looser workplaces, our rights such as fair income, equal right to speak, and humane working conditions are taken from our hands</strong>. It is so exhausting for us to struggle for our rights in those corporations which are all organized from top to bottom
  </p>
  <p>
    As we reject this kind of working life, we try to work freelance or we quit whenever we get depressed. So, we work temporarily, but this prevents us from living a sustainable life. Loneliness and lack of social and job security consumes us from the inside while we freelance,and temporary jobs make us feel maladapted and alienated. On the other hand, we know that the problem is not with us, but with the working conditions and we want to change it.
  </p>
  <p>
    <strong>In Bağımsız Atölye (Independent Workshop), we want to do away with unfairness and alienation in the work places while we try to find a social solution to the individualizing conditions of freelancing.</strong>
  </p>
  <p>
    <strong>As workers coming from different industries, our purpose is to work free from hierarchy and make our own decisions.</strong> As a result, we aim to be take resposnsibility for our own living and produce social benefit at the same time. We want to act with solidarity with communities with similar concerns and aims by sharing our experiences. Also, we will prioritize producing social benefit because we know our aims are related to others’ concerns and aims.
  </p>
</div>
@endsection
