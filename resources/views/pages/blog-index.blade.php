@extends('layouts.app')

@section('header')
  {!! text('blog_title', true) !!}
  <hr class="my-0">
  <span class="sub-title d-block">
    {{ isset($tag) ? $tag->title : text('blog_title_sub') }}
  </span>
@endsection

@section('content')
  <div class="container page-blog page-blog-index mt-5">
    <div class="accordion" id="blogAccordion">
      @foreach($blogList as $blog)
        <div class="card">
          <div
            class="card-header collapsed"
            id="blogHeading{{ $blog->id }}"
            data-toggle="collapse"
            data-target="#blog{{ $blog->id }}"
            aria-expanded="false"
            aria-controls="blog{{ $blog->id }}"
          >
            <h2 class="mb-0">
              <button
                class="btn btn-link collapsed"
                type="button"
              >{{ $blog->title }}</button>

              <i class="fal fa-plus float-right"></i>
              <i class="fal fa-times float-right"></i>

              <time datetime="{{ $blog->published_at }}" class="float-right">
                {{ format_date_fe($blog->published_at) }}
              </time>
            </h2>

          </div>

          <div
            id="blog{{ $blog->id }}"
            class="collapse"
            aria-labelledby="blogHeading{{ $blog->id }}"
            data-parent="#blogAccordion"
          >
            <div class="card-body">
              <a class="p-3" href="{{ ba_route('blog.show', $blog->slug) }}">
                <div class="row">
                  <div class="col-lg-4 thumb">
                    <img
                      class="img-fluid"
                      src="{{ $blog->cover->url_thumb }}"
                      alt="{{ $blog->title }}"
                    >
                  </div>
                  <p class="col-lg-8 text mb-0 mt-0">
                    {{ str_limit(strip_tags($blog->body), 280) }}
                  </p>
                </div>
                <div class="author">
                  {{ $blog->author->name }}
                </div>
                <div class="detail-link">
                  {{ __('Devamı') }}&nbsp;&nbsp;
                </div>
              </a>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
@endsection
