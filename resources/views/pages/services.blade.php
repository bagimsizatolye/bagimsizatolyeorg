@extends('layouts.app')

@section('header')
  {!! text('services_title', true) !!}
  <hr clasS="my-0">
  <span class="sub-title d-block">{!! text('services_title_sub', true) !!}</span>
@endsection

@section('content')
<div class="container container-reading services">

  <h3>{{ text('services_text_title') }}</h3>

  {!! text('services_text', true) !!}
</div>

<div class="services-banner mb-5">
  <img src="{{ asset('img/services-banner-alien.png') }}" class="image">
  <div class="services-banner-content container pt-5">
    <div class="row justify-content-around">
      @foreach($users as $user)
        <div class="col-12 col-lg-3 col-xxl-2">
          <div class="row">
            <div class="col-3 col-lg-12 mb-4">
              @if($user->photo)
                <img src="{{ $user->photo->url_thumb }}" alt="{{ $user->name }}" class="img-fluid rounded-circle">
              @endif
            </div>
            <div class="col-9 col-lg-12 text-lg-center">
              <strong>{{ $user->name }}</strong>
              <p>
                {{ $user->title }}
              </p>
              <p>
                @if(!empty($user->description))
                  {{ $user->description }}
                @else
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non sollicitudin odio, ut tincidunt nibh. Proin convallis porta pellentesque. Donec eget lobortis felis. Integer imperdiet, erat et efficitur placerat, nisi eros dapibus turpis, eu lobortis urna lacus ac urna.
                @endif
              </p>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>

<div class="home-portfolio text-center">
  <h3 class="text-bold mb-0">{{ __('İşlerimiz') }}</h3>

  <hr class="my-5">

  <portfolio-list-component
    portfolio-json="{!! str_replace('"', '\'', json_encode($list->toArray(), JSON_HEX_APOS | JSON_HEX_QUOT)) !!}"
    v-bind:show-categories="false"
    container-class="container"
  ></portfolio-list-component>
</div>
@endsection
