@extends('layouts.app')

@section('header')
@endsection

@section('sky')
  <div class="sky-sm-bg"></div>
  <div class="fog-sm-bg"></div>

  @foreach($adminList as $admin)
    <div class="star star-{{ $loop->iteration }} star-sm"></div>
  @endforeach
@endsection

@section('content')
  <div class="container-fluid portfolio-show mt-5">
    <article class="row text-center text-lg-left">
      <div class="col-12 col-lg-2">
        <div class="row">
          <section class="col-12 col-md-4 col-lg-12 meta-box">
            <div class="meta-box-title">
              {{ __('Müşteri:') }}
            </div>
            <div class="meta-box-content">
              {{ $portfolio->customer_name }}
            </div>
            @if(!empty($portfolio->link))
              <div class="meta-box-content meta-box-content-link">
                <a href="{{ $portfolio->link }}" target="_blank">
                  {{ $portfolio->link }}
                </a>
              </div>
            @endif
          </section>
          <section class="col-12 col-md-4 col-lg-12 meta-box">
            <div class="meta-box-title">
              {{ __('İş Tanımı:') }}
            </div>
            <div class="meta-box-content">
              {{ $portfolio->description }}
            </div>
          </section>
          @if(!empty($portfolio->published_at))
            <section class="col-12 col-md-4 col-lg-12 meta-box">
              <div class="meta-box-title">
                {{ __('Tarih:') }}
              </div>
              <div class="meta-box-content">
                {{ format_date_fe($portfolio->published_at, $portfolio->published_at_format) }}
              </div>
            </section>
          @endif
          <section class="col-12 col-md-4 col-lg-12 meta-box">
            <div class="meta-box-title">
              {{ __('Kategori:') }}
            </div>
            <div class="meta-box-content">
              {{ $portfolio->workCategories->implode('title', ', ') }}
            </div>
          </section>
          <div class="col-12 meta-box mt-4">
            <div class="meta-box-title">
              {{ __('Üreticiler:') }}
            </div>
          </div>
          @foreach($portfolio->users as $user)
            <div class="col col-xl-12 text-center text-xl-left">
              <address class="user-box mb-4">
                <div class="user-box-image mb-2">
                  <img src="{{ $user->photo->url_thumb }}" class="rounded-circle">
                </div>
                <div class="user-box-name mb-1">
                  {{ $user->name }}
                </div>
                <div class="user-box-title">
                  {{ $user->title }}
                </div>
              </address>
            </div>
          @endforeach
        </div>
      </div>
      <div class="col-12 col-lg-10">
        @if($portfolio->view_type_id === 'video')
          <section class="video-container mb-5">
            <video
              controls
              preload="auto"
              @if($portfolio->media->count() > 0)
                poster="{{ $portfolio->media->first()->url }}"
              @endif
            >
              <source src="{{ $portfolio->video->url }}" type="video/mp4"></source>
            </video>
          </section>
        @else
          <section class="slider-container mb-5">
            <img class="img-fluid" src="{{ $portfolio->media->first()->url }}" alt="{{ $portfolio->title }}">
            <div class="background"></div>
            <div class="arrow arrow-right"></div>
            <div class="arrow arrow-left"></div>
            <div class="media-count">
              {{ __(':count görsel', ['count' => $portfolio->media->count()]) }}
            </div>
          </section>

          @include('partials.gallery', [
            'photos' => $portfolio->media,
            'galleryId' => 'portfolioGallery' . $portfolio->id,
            'selector' => '.slider-container, .slider-container *',
            'width' => 1600,
            'height' => 900,
          ])
        @endif

        <header>
          <h3>{{ $portfolio->title }}</h3>
        </header>

        <section class="portfolio-body mb-5">
          {!! $portfolio->body !!}
        </section>
      </div>
    </article>

    <div class="portfolio-banner">
      <img class="ufo" src="{{ asset('img/portfolio-banner-ufo.png') }}">

      <div class="button-container">
        <div class="row align-items-center">
          <div class="col">
            <a href="{{ ba_route('contact', ['ref' => 'portfolio-' . $portfolio->id]) }}" class="btn btn-lg btn-primary">
              <i class="fas fa-chevron-double-right"></i>
              {{ __('İLETİŞİME GEÇ') }}
              <i class="fas fa-chevron-double-left"></i>
            </a>
          </div>
        </div>
      </div>
    </div>

    @if($portfolio->relatedPortfolios->count())
      <h3 class="mt-5">{{ __('Benzer İşlerimiz') }}</h3>

      <div class="related-portfolio mt-5">
        <div class="row">
          @foreach($portfolio->relatedPortfolios as $item)
            @if($item->thumbnail)
              @include('partials.portfolio-mixed', ['item' => $item])
            @endif
          @endforeach
        </div>
      </div>
    @endif
  </div>
@endsection

@section('scripts')
  @parent
@endsection
