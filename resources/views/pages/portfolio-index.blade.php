@extends('layouts.app')

@section('header')
  {!! text('portfolio_title', true) !!}
  <hr clasS="my-0">
  <span class="sub-title d-block">{!! text('portfolio_title_sub', true) !!}</span>
@endsection

@section('content')
  <portfolio-list-component
    portfolio-json="{!! str_replace('"', '\'', json_encode($list->toArray(), JSON_HEX_APOS | JSON_HEX_QUOT)) !!}"
    v-bind:show-categories="true"
  ></portfolio-list-component>
  <div class="container-fluid portfolio-index mt-5" id="portfolioListDummy">
    <div class="categories">
      @foreach($workCategories as $category)
        <div class="category-item">
          <img src="{{ $category->icon->url_thumb }}">
          <br>
          {{ $category->title }}
        </div>
      @endforeach
    </div>

    <div class="row">
      @foreach($list as $item)
        @if($item->thumbnail)
          @if(in_array($item->view_type_id, ['image', 'video']))
            @include('partials.portfolio-image', ['item' => $item])
          @else
            @include('partials.portfolio-' . $item->view_type_id, ['item' => $item])
          @endif
        @endif
      @endforeach
    </div>
  </div>
@endsection

@section('scripts')
  @parent

  <script>
    // window.portfolioList = {!! json_encode($list->toArray(), JSON_HEX_APOS) !!}
    window.categories = {!! json_encode($workCategories->toArray(), JSON_HEX_APOS) !!}
  </script>
@endsection
