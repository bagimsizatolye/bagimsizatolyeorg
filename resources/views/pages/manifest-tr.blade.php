@extends('layouts.app')

@section('header')
  {!! text('manifest_title', true) !!}
  <hr clasS="my-0">
  <span class="sub-title d-block">{!! text('manifest_title_sub', true) !!}</span>
@endsection

@section('content')
<div class="container container-reading manifest">

  <h3>{{ __('MANİFESTO') }}</h3>

  <p>
    Hayatımızın en verimli dakikalarını işyerlerimizde harcıyoruz. Yaşamımızı kazanabilmek için çalışmaya devam etmemiz gerekiyor. Ancak <strong>gerek kurumsal, gerekse nispeten daha serbest işyerlerinde, adil kazanç, eşit söz hakkı, insani çalışma şartları gibi haklarımız elimizden alınıyor</strong>. Bu haklarımız için yukarıdan aşağı örgütlenmiş şirketlerde mücadele etmek ise hepimizi oldukça zorluyor.
  </p>
  <p>
    Biz, bu iş hayatını reddedenler olarak, ya bağımsız freelance çalışmaya çalışıyoruz ya da her bunaldığımızda istifa ederek geçiçi işlerde çalışıyoruz. Ancak bunlar bizim sürdürülebilir bir hayat yaşamamızı engelliyor. Freelance çalışırken yalnızlık ve güvencesizlik bizi içten içe tüketirken, geçici işler bizi kendimize yabancılaştırıyor ve bizi uyumsuz hissettiriyor. Ancak, bir yanımızla, sorunun bizde değil çalışma ortamlarında olduğunu biliyoruz ve artık bunu değiştirmek istiyoruz.
  </p>
  <p>
    <strong>Bağımsız Atölye'de hem işyerlerindeki adaletsizliği ve yabancılaşmayı ortadan kaldırmayı hem de freelance çalışmanın bireyselliğine toplumsal bir çare bulmayı hedefliyoruz.</strong>
  </p>
  <p>
    Amacımız <strong>farklı disiplinlerden insanların, ast-üst ilişkisinden bağımsız, kendi kararlarını vererek bir araya gelmesi ve bunun sonucunda sorumluluk sahibi olarak, hem geçimimizi sağlamak hem de toplumsal fayda üretmektir</strong>. Denediğimiz çalışma yöntemini açık bir şekilde paylaşarak, bizimle benzer dertleri ve hedefleri olan topluluklarla dayanışmak istiyoruz. Ayrıca bu hedeflerimizin başkalarının da sorunları ve amaçlarıyla ilişkili olduğunun bilincinde olarak, toplumsal fayda üretmeyi önceleyeceğiz.
  </p>
</div>
@endsection
