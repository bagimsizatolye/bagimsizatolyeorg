@extends('layouts.app')

@section('header')
  {!! text('contact_title', true) !!}
  <hr clasS="my-0">
  <span class="sub-title d-block">{!! text('contact_title_sub', true) !!}</span>
@endsection

@section('content')
  <div class="container contact-page mt-5">
    <p class="intro-text text-center mx-auto mb-5">
      {{ text('contact_text') }}
    </p>

    <div class="row">
      <div class="col-12 col-md-4 col-lg-3">
        <div class="p-3 p-xxl-5">
          <div class="mb-5">
            <img src="img/logo-dark.svg" alt="Bağımsız Atölye Logo" class="img-fluid logo logo-footer">
          </div>

          <div class="contact">
            <div class="row row-condensed">
              <div class="col-2">
                <div class="mb-3">
                  <i class="fal fa-hand-point-up"></i>
                </div>
                <div class="mb-3">
                  <i class="fal fa-phone fa-flip-horizontal"></i>
                </div>
                <div class="mb-3">
                  <i class="fal fa-envelope"></i>
                </div>
              </div>
              <div class="col-1 vertical-line"></div>
              <div class="col-8">
                <div class="mb-3">
                  <a href="https://t.me/{{ config('ba.contact_telegram') }}" target="_blank">
                    <i class="fab fa-telegram-plane"></i>
                  </a>
                  &nbsp;
                  <a
                    href="https://api.whatsapp.com/send?phone={{ config('ba.contact_phone') }}"
                    target="_blank"
                  ><i class="fab fa-whatsapp"></i></a>
                </div>
                <div class="mb-3">
                  <a
                    class="d-sm-none lh-fix"
                    href="tel:+{{ config('ba.contact_phone') }}"
                  >+{{ config('ba.contact_phone') }}</a>
                  <span class="d-none d-sm-inline">
                    +{{ config('ba.contact_phone') }}
                  </span>
                </div>
                <div class="mb-3">
                  <a class="lh-fix" href="mailto:hayal@bagimsizatolye.org">
                    {{ config('ba.admin_email') }}
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-7 col-lg-8 offset-md-1">
        @include('partials.notifications')

        <form class="footer-contact" action="{{ ba_route('contact') }}" method="post">
          {{ csrf_field() }}

          <input
            type="email"
            name="email_check"
            class="d-none"
          >

          <div class="input-group mb-5">
            <div class="input-group-prepend">
              <label for="name">{{ __('Ad soyad') }}:</label>
            </div>
            <input
              type="text"
              id="name"
              name="name"
              class="form-control"
              maxlength="250"
              required
              value="{{ old('name') }}"
            >
          </div>

          <div class="input-group mb-5">
            <div class="input-group-prepend">
              <label for="email">{{ __('E-posta adresiniz') }}:</label>
            </div>
            <input
              type="email"
              id="email"
              name="email"
              class="form-control"
              maxlength="250"
              required
              value="{{ old('email') }}"
            >
          </div>

          <div class="input-group mb-5">
            <div class="input-group-prepend">
              <label for="subject">{{ __('Konu') }}:</label>
            </div>
            <input
              type="text"
              id="subject"
              name="subject"
              class="form-control"
              maxlength="250"
              required
              value="{{ old('subject') }}"
            >
          </div>

          <div class="input-group mb-5">
            <div class="input-group-prepend">
              <label for="text">{{ __('Mesaj') }}</label>
            </div>
            <textarea
              id="text"
              name="text"
              class="form-control"
              maxlength="10000"
              required
              rows="8"
            >{{ old('text') }}</textarea>
          </div>

          <button class="btn btn-primary btn-block mb-5" type="submit">{{ __('GÖNDER') }}</button>
        </form>
      </div>
    </div>
  </div>
@endsection
