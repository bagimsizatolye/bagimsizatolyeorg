@extends('layouts.app')

@section('header')
  {{ $blog->title }}
  <hr class="my-0">
  <span class="sub-title d-block">{{ $blog->author->name }}</span>
@endsection

@section('content')
  <div class="container container-reading page-blog page-blog-show mt-5">
    <article>
      <section>
        <img
          class="img-fluid mb-3"
          alt="{{ $blog->title }}"
          src="{{ $blog->cover->url }}"
        >

        <div class="text-primary text-center text-lg-right text-uppercase font-weight-normal text-sm mb-3">
          <time datetime="{{ $blog->published_at }}">
            {{ format_date_fe($blog->published_at) }}
          </time>
          ∙
          <address class="d-inline">
            {{ $blog->author->name }}
          </address>
        </div>
      </section>

      <header>
        <h1 class="text-uppercase font-weight-bold mb-3">{{ $blog->title }}</h1>
      </header>

      <section class="mb-5">
        {!! $blog->body !!}
      </section>

      <section class="tags mb-5">
        <strong>{{ __('Etiketler:') }}</strong>
        @foreach($blog->tags as $tag)
          <a href="{{ ba_route('blog.tag.show', $tag->slug) }}">
            <span class="badge badge-light">{{ $tag->title }}</span>
          </a>
        @endforeach
      </section>
    </article>

    <div class="share mb-5">
      <strong>{{ __('Paylaş:') }}</strong>
      <a
        href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(ba_route('blog.show', $blog->slug)) }}"
        target="_blank"
        title="{{ __('Facebook\'ta paylaş') }}"
      ><i class="fab fa-facebook-f fa-lg pr-2"></i></a>
      <a
        href="https://twitter.com/intent/tweet?text={{ urlencode(ba_route('blog.show', $blog->slug)) }}"
        target="_blank"
        title="{{ __('Twitter\'da paylaş') }}"
      ><i class="fab fa-twitter fa-lg px-1"></i></a>
    </div>

    @if($latestBlogList->count() !== 0)
      <div class="latest-blogs">
        <h3 class="mb-4">{{ __('Son Yazılar:') }}</h3>

        <div class="row">
          @foreach($latestBlogList as $latestBlog)
            <div class="col-12 col-md-4">
              <a class="d-block mb-3" href="{{ ba_route('blog.show', $latestBlog->slug) }}">
                <article class="latest-blog">
                  <img
                    src="{{ $latestBlog->cover->url_thumb }}"
                    alt="{{ $latestBlog->title }}"
                    class="img-fluid mb-2"
                  >

                  <h3 class="mb-2">{{ $latestBlog->title }}</h3>

                  <time class="d-none" datetime="{{ $latestBlog->published_at }}">
                    {{ format_date_fe($latestBlog->published_at) }}
                  </time>

                  <p class="mb-2">
                    {{ str_limit(strip_tags($latestBlog->body), 100) }}
                  </p>
                </article>
              </a>
            </div>
          @endforeach
        </div>
      </div>
    @endif
  </div>
@endsection
