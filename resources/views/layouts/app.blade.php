<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ $meta->seo->title }}</title>

  <base href="/">

  <meta name="keywords" content="{{ $meta->seo->keywords }}">
  <meta name="description" content="{{ $meta->seo->description }}">

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:creator" content="@bagimsizatolye">
  <meta name="twitter:site" content="@bagimsizatolye">
  <meta name="twitter:title" content="{{ $meta->seo->title }}">

  @if($meta->seo->abstract)
    <meta name="abstract" content="{{ $meta->seo->abstract }}">
  @endif
  @if($meta->seo->newsKeywords)
    <meta name="news_keywords" content="{{ $meta->seo->newsKeywords }}">
  @endif

  @if(!empty($meta->seo->ogDescription))
    <meta property="og:description" content="{{ $meta->seo->ogDescription }}">
    <meta name="twitter:description" content="{{ $meta->seo->ogDescription }}">
  @else
    <meta property="og:description" content="{{ $meta->seo->description }}">
    <meta name="twitter:description" content="{{ $meta->seo->description }}">
  @endif

  @if(!empty($meta->seo->image))
    <meta property="og:image" content="{{ $meta->seo->image }}">
    <meta name="twitter:image:src" content="{{ $meta->seo->image }}">
  @endif

  @if(!empty($meta->seo->imageWidth))
    <meta property="og:image:width" content="{{ $meta->seo->imageWidth }}">
    <meta name="twitter:image:width" content="{{ $meta->seo->imageWidth }}">
  @endif
  @if(!empty($meta->seo->imageHeight))
    <meta property="og:image:height" content="{{ $meta->seo->imageHeight }}">
    <meta name="twitter:image:height" content="{{ $meta->seo->imageHeight }}">
  @endif

  <meta property="og:type" content="website">
  <meta property="og:title" content="{{ $meta->seo->title }}">
  <meta property="og:site_name" content="{{ config('app.name') }}">
  <meta property="og:url" content="{{ $meta->seo->url }}">

  @if(!empty($meta->seo->robots))
    <meta name="robots" content="{{ $meta->seo->robots }}">
  @endif

  @if(!empty($meta->seo->canonical))
    <link rel="canonical" content="{{ $meta->seo->canonical }}">
  @else
    <link rel="canonical" content="{{ request()->url() }}">
  @endif

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}">
  <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
</head>
<body>
  <div id="app">
    <nav class="navbar navbar-expand-lg navbar-dark navbar-ba fixed-top">
      <div class="container-fluid align-items-lg-start">
        <a class="navbar-brand" href="{{ url('/') }}">
          <img src="img/logo.svg" alt="Bağımsız Atölye Logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Left Side Of Navbar -->
          <ul class="navbar-nav mr-auto">

          </ul>

          <!-- Right Side Of Navbar -->
          <ul class="navbar-nav ml-auto mt-md-1">
            <li class="nav-item">
              <a
                class="nav-link {{ request()->routeIs('manifest') ? 'active' : '' }}"
                href="{{ ba_route('manifest') }}"
              >{{ __('MANİFESTO') }}</a>
            </li>
            <li class="nav-item">
              <a
                class="nav-link {{ request()->routeIs('services') ? 'active' : '' }}"
                href="{{ ba_route('services') }}"
              >{{ __('MAHARETLERİMİZ') }}</a>
            </li>
            <li class="nav-item">
              <a
                class="nav-link {{ request()->routeIs('portfolio.*') ? 'active' : '' }}"
                href="{{ ba_route('portfolio.index') }}"
              >{{ __('İŞLERİMİZ') }}</a>
            </li>
            <li class="nav-item">
              <a
                class="nav-link {{ request()->routeIs('blog.*') ? 'active' : '' }}"
                href="{{ ba_route('blog.index') }}"
              >{{ __('SEYİR DEFTERİ') }}</a>
            </li>
            <li class="nav-item">
              <a
                class="nav-link {{ request()->routeIs('contact') ? 'active' : '' }}"
                href="{{ ba_route('contact') }}"
              >{{ __('İLETİŞİM') }}</a>
            </li>

            <li class="nav-item dropdown">
              <a
                id="languageDropdown"
                class="nav-link dropdown-toggle no-border"
                href="#"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                v-pre
              >{{ strtoupper(app()->getLocale()) }} <span class="caret"></span></a>

              <div
                class="dropdown-menu dropdown-menu-right"
                aria-labelledby="languageDropdown"
              >
                @foreach(config('ba.available_languages') as $lang)
                  <a
                    class="dropdown-item"
                    href="{{ ba_get_route_in_locale(request(), $lang) }}"
                  >{{ strtoupper($lang) }}</a>
                @endforeach
              </div>
            </li>

            <!-- Authentication Links -->
            @guest
              <!-- <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
              </li>
              @if (Route::has('register'))
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
              @endif -->
            @else
              <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                  {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                  </form>
                </div>
              </li>
            @endguest
          </ul>
        </div>
      </div>
    </nav>

    <main class="pb-4">
      <header>
        @section('sky')
          <div class="sky-bg"></div>
          <div class="fog-bg"></div>

          @foreach($adminList as $admin)
            <div class="star star-{{ $loop->iteration }}"></div>
          @endforeach
        @show

        <div class="header-text-container text-center">
          <span class="header-text">
            @yield('header')
          </span>
        </div>
      </header>
      <!-- <img class="header-bg" src="img/home-bg.jpg" class="img-fluid">
      <img class="header-bg-fog" src="img/bg-fog.svg" class="img-fluid"> -->

      @yield('content')
    </main>

    <footer class="text-center text-md-left">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-6 col-xl-3">
            <div class="p-3 p-xxl-5">
              <div class="mb-5">
                <img src="img/logo-footer.svg" alt="Bağımsız Atölye Logo" class="img-fluid logo logo-footer">
              </div>

              <div class="contact">
                <div class="row row-condensed">
                  <div class="col-2">
                    <div class="mb-3">
                      <i class="fal fa-hand-point-up"></i>
                    </div>
                    <div class="mb-3">
                      <i class="fal fa-phone fa-flip-horizontal"></i>
                    </div>
                    <div class="mb-3">
                      <i class="fal fa-envelope"></i>
                    </div>
                  </div>
                  <div class="col-1 vertical-line"></div>
                  <div class="col-8">
                    <div class="mb-3">
                      <a href="https://t.me/{{ config('ba.contact_telegram') }}" target="_blank">
                        <i class="fab fa-telegram-plane"></i>
                      </a>
                      &nbsp;
                      <a
                        href="https://api.whatsapp.com/send?phone={{ config('ba.contact_phone') }}"
                        target="_blank"
                      ><i class="fab fa-whatsapp"></i></a>
                    </div>
                    <div class="mb-3">
                      <a
                        class="d-sm-none lh-fix"
                        href="tel:+{{ config('ba.contact_phone') }}"
                      >+{{ config('ba.contact_phone') }}</a>
                      <span class="d-none d-sm-inline">
                        +{{ config('ba.contact_phone') }}
                      </span>
                    </div>
                    <div class="mb-3">
                      <a class="lh-fix" href="mailto:hayal@bagimsizatolye.org">
                        {{ config('ba.admin_email') }}
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6 col-xl-3">
            <div class="p-3 p-xxl-5">
              <h5 class="mb-3">{{ __('ATÖLYE') }}</h5>

              <ul class="footer-menu">
                <li>
                  <a href="{{ ba_route('manifest') }}">{{ __('MANİFESTO') }}</a>
                </li>
                <li>
                  <a href="{{ ba_route('services') }}">{{ __('MAHARETLERİMİZ') }}</a>
                </li>
                <li>
                  <a href="{{ ba_route('portfolio.index') }}">{{ __('İŞLERİMİZ') }}</a>
                </li>
                <li>
                  <a href="{{ ba_route('blog.index') }}">{{ __('SEYİR DEFTERİ') }}</a>
                </li>
                <li>
                  <a href="{{ ba_route('contact') }}">{{ __('İLETİŞİM') }}</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-12 col-md-6 col-xl-3">
            <div class="p-3 p-xxl-5">
              <h5 class="mb-3">{{ __('SEYİR DEFTERİ') }}</h5>

              <ul class="footer-menu">
                @foreach($latestBlogList as $blog)
                  <li>
                    <a
                      class="text-uppercase"
                      href="{{ ba_route('blog.show', $blog->slug) }}"
                    >{{ $blog->title }}</a>
                  </li>
                @endforeach
              </ul>
            </div>
          </div>
          <div class="col-12 col-md-6 col-xl-3">
            <div class="p-3 p-xxl-5">
              <h5 class="mb-3">{{ __('MESAJ') }}</h5>

              <form
                class="footer-contact form-dark"
                action="{{ ba_route('contact') }}"
                method="post"
              >
                @include('partials.notifications')

                {{ csrf_field() }}

                <input type="hidden" name="subject" value="Footer iletişim formu">
                <input
                  type="email"
                  name="email_check"
                  class="d-none"
                >

                <div class="form-group">
                  <label for="name" class="sr-only">{{ __('AD SOYAD') }}</label>
                  <input
                    class="no-padding"
                    type="text"
                    id="name"
                    name="name"
                    placeholder="{{ __('AD SOYAD') }}"
                    required
                    value="{{ old('name') }}"
                  >
                </div>

                <div class="form-group">
                  <label for="email" class="sr-only">{{ __('E-POSTA') }}</label>
                  <input
                    class="no-padding"
                    type="email"
                    id="email"
                    name="email"
                    placeholder="{{ __('E-POSTA') }}"
                    required
                    value="{{ old('email') }}"
                  >
                </div>

                <div class="form-group">
                  <label for="text" class="sr-only">{{ __('MESAJ') }}</label>
                  <textarea
                    class="no-padding"
                    id="text"
                    name="text"
                    placeholder="{{ __('MESAJ') }}"
                    required
                  >{{ old('text') }}</textarea>
                </div>

                <div class="text-right">
                  <button class="btn btn-primary btn-sm btn-block" type="submit">
                    <i class="fas fa-chevron-double-right"></i>
                    {{ __('GÖNDER') }}
                    <i class="fas fa-chevron-double-left"></i>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div class="row footer-two pb-3">
          <div class="col-12 col-md-6">
            <div class="pl-3 pl-xxl-5">
              <strong>
                {{ date('Y') }}
                Bağımsız Atölye
                <span class="d-inline-block" style="transform: rotate(180deg)">&copy;</span>
              </strong>
            </div>
          </div>
          <div class="col-12 col-md-6 text-center text-md-right">
            <a href="https://facebook.com/bagimsizatolye" target="_blank">
              <i class="fab fa-facebook-f"></i>
            </a>
            <a href="https://twitter.com/bagimsizatolye" target="_blank">
              <i class="fab fa-twitter"></i>
            </a>
            <a href="https://instagram.com/bagimsizatolye" target="_blank">
              <i class="fab fa-instagram"></i>
            </a>
          </div>
        </div>
      </div>
    </footer>

    <a href="#" class="go-to-top">
      <img src="{{ asset('images/rocket-top.svg') }}" alt="{{ __('Yukarı çık') }}">
      <br>
      {{ __('Yukarı çık') }}
    </a>
  </div>

  @yield('scripts')

  @if(config('services.matomo.status') and !auth()->check())
    <!-- Matomo -->
    <script type="text/javascript">
      var _paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(["setDomains", ["{{ config('services.matomo.domains') }}"]]);
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      _paq.push(['enableHeartBeatTimer']);
      (function() {
        var u="{{ config('services.matomo.host') }}";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '{{ config('services.matomo.site_id') }}']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
    <!-- End Matomo Code -->
  @endif
</body>
</html>
