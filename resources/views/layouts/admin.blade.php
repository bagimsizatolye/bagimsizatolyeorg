<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <base href="/">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/admin.js') }}" defer></script>

  <!-- Styles -->
  <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
  <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}">
  <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
</head>
<body>
  <div id="app">
    <nav class="navbar-admin">
      <div class="p-4">
        <a href="{{ route('home') }}">
          <img src="img/logo-footer.svg" alt="Bağımsız Atölye Logo" class="img-fluid logo">
        </a>
      </div>
      <ul>
        <li class="{{ request()->routeIs('admin.contactform.*') ? 'active' : '' }}">
          <a href="{{ route('admin.contactform.index') }}">{{ __('İletişim') }}</a>
        </li>
        <li class="{{ request()->routeIs('admin.proposal.*') ? 'active' : '' }}">
          <a href="{{ route('admin.proposal.index') }}">{{ __('Teklifler') }}</a>
        </li>
        <li class="{{ request()->routeIs('admin.portfolio.*') ? 'active' : '' }}">
          <a href="{{ route('admin.portfolio.index') }}">{{ __('İşlerimiz') }}</a>
        </li>
        <li class="{{ request()->routeIs('admin.work-category.*') ? 'active' : '' }}">
          <a href="{{ route('admin.work-category.index') }}">{{ __('Maharetlerimiz') }}</a>
        </li>
        <li class="{{ request()->routeIs('admin.user.*') ? 'active' : '' }}">
          <a href="{{ route('admin.user.index') }}">{{ __('Biz') }}</a>
        </li>
        <li class="{{ request()->routeIs('admin.blog.*') ? 'active' : '' }}">
          <a href="{{ route('admin.blog.index') }}">{{ __('Seyir Defteri') }}</a>
        </li>
        <li class="{{ request()->routeIs('admin.text.*') ? 'active' : '' }}">
          <a href="{{ route('admin.text.index') }}">{{ __('Metinler') }}</a>
        </li>
      </ul>
    </nav>

    <main class="p-3">
      <header>
        @yield('header')
      </header>

      @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          {{ session('success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif

      @if(isset($errors) && $errors->count())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <ul>
            @foreach($errors->toArray() as $error)
              <li>{{{ $error[0] }}}</li>
            @endforeach
          </ul>
        </div>
      @endif

      @yield('content')
    </main>
  </div>
</body>
</html>
