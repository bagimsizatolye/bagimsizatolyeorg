@extends('layouts.admin')

@section('header')
  <h3>
    {{ $workCategory->id ? $workCategory->title : __('Yeni Maharet Ekle') }}
    - <em>{{ strtoupper($lang) }}</em>
  </h3>
@endsection

@section('content')
  {!! Form::open([
    'url' => $workCategory->id ?
      route('admin.work-category.update', $workCategory->id) :
      route('admin.work-category.store'),
    'method' => $workCategory->id ? 'PUT' : 'POST',
    'files' => true,
    ]) !!}

    <input type="hidden" name="lang" value="{{ $lang }}">

    <div class="form-group">
      <label for="title">{{ __('Başlık') }}</label>
      <input
        type="text"
        name="title"
        class="form-control"
        id="title"
        maxlength="255"
        required
        value="{{ old('title') ?: $workCategory->getTranslation('title', $lang, false) }}"
        autocomplete="off"
      >
    </div>

    <div class="form-group">
      <label for="body">{{ __('Metin') }}</label>
      <textarea
        name="body"
        class="form-control summernote-init"
        id="body"
        required
      >{!! old('body') ?: $workCategory->getTranslation('body', $lang, false) !!}</textarea>
    </div>

    <div class="form-group">
      <label for="icon">{{ __('Küçük resim') }}</label>
      <input
        class="form-control"
        id="icon"
        name="icon"
        type="file"
        accept="image/*"
        multiple
      >
      <p class="help-block">
        Kare olmalı. PNG ve SVG kabul.
      </p>

       @if($workCategory->id and $workCategory->icon)
         Mevcut resim: <img src="{{ $workCategory->icon->url }}" style="max-height: 40px;">
       @endif
    </div>

    <div class="text-right">
      <button type="submit" class="btn btn-outline-primary">{{ __('Kaydet') }}</button>
    </div>

  {!! Form::close() !!}

@endsection
