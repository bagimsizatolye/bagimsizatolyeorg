@extends('layouts.admin')

@section('header')
  <div class="row">
    <div class="col-12 col-md-8">
      <h3>{{ __('Maharetlerimiz') }}</h3>
    </div>
    <div class="col-12 col-md-4 text-xs-center text-md-right">
      <a class="btn btn-outline-dark" href="{{ route('admin.work-category.create') }}">
        {{ __('Yeni Maharet Ekle') }}
      </a>
    </div>
  </div>
@endsection

@section('content')
  <table class="table table-striped">
    <thead>
      <tr>
        <th>{{ __('Başlık') }}</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $item)
        <tr>
          <td>{{ $item->title }}</td>
          <td class="text-right">
            @foreach(config('ba.available_languages') as $lang)
              <a
                class="btn btn-outline-info btn-sm"
                href="{{ route('admin.work-category.edit', [
                  $item->id,
                  'lang' => $lang,
                  ]) }}"
              >{{ __('Düzenle') }} - <strong>{{ strtoupper($lang) }}</strong></a>
            @endforeach
            {{ Form::open([
              'url' => route('admin.work-category.destroy', $item->id),
              'method' => 'DELETE',
              'class' => 'd-inline',
              ]) }}
              <button type="submit" class="btn btn-outline-danger btn-sm" data-ask="{{ __('Silmek istediğinden emin misin?') }}">
                {{ __('Sil') }}
              </button>
            {{ Form::close() }}
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  @if($list->count() === 0)
    <p class="text-center">
      {{ __('Hiçbir şey eklenmemiş.') }}
    </p>
  @endif
@endsection
