@extends('layouts.admin')

@section('header')
  <div class="row">
    <div class="col-12 col-md-8">
      <h3>bagimsizatolye.org yönetici</h3>
    </div>
    <div class="col-12 col-md-4 text-xs-center text-md-right">
      <a class="btn btn-outline-dark" href="{{ route('admin.portfolio.create') }}">
        {{ __('Yeni İş Ekle') }}
      </a>
    </div>
  </div>
@endsection

@section('content')
  <table class="table table-striped">
    <thead>
      <tr>
        <th style="width: 60px;"></th>
        <th>{{ __('Başlık') }}</th>
        <th>{{ __('Kategoriler') }}</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $item)
        <tr>
          <td>
            @if($item->thumbnail)
              <img src="{{ $item->thumbnail->url_thumb }}" style="height: 30px;">
            @endif
          </td>
          <td><a href="{{ route('portfolio.show', $item->slug) }}">{{ $item->title }}</a></td>
          <td>
            @if($item->workCategories->count() > 0)
              {{ $item->workCategories->pluck('title')->join(', ') }}
            @endif
          </td>
          <td class="text-right">
            {{ Form::open([
              'url' => route('admin.portfolio.update', $item->id),
              'method' => 'PUT',
              'class' => 'd-inline',
              ]) }}
              @if(empty($item->featured_at))
                <input type="hidden" name="featured_at" value="{{ now() }}">
                <input type="hidden" name="lang" value="{{ config('app.locale') }}">
                <button
                  type="submit"
                  class="btn btn-outline-success btn-sm"
                  title="{{ __('Öne çıkar') }}"
                ><i class="far fa-star"></i></button>
              @else
                <input type="hidden" name="featured_at" value="">
                <input type="hidden" name="lang" value="{{ config('app.locale') }}">
                <button
                  type="submit"
                  class="btn btn-success btn-sm"
                  title="{{ __('Öne çıkanlardan kaldır') }}"
                ><i class="fas fa-star"></i></button>
              @endif
            {{ Form::close() }}

            {{ Form::open([
              'url' => route('admin.portfolio.update', $item->id),
              'method' => 'PUT',
              'class' => 'd-inline',
              ]) }}
              @if(!$item->is_visible)
                <input type="hidden" name="is_visible" value="1">
                <input type="hidden" name="lang" value="{{ config('app.locale') }}">
                <button
                  type="submit"
                  class="btn btn-outline-success btn-sm"
                  title="{{ __('Göster') }}"
                ><i class="far fa-eye-slash"></i></button>
              @else
                <input type="hidden" name="is_visible" value="0">
                <input type="hidden" name="lang" value="{{ config('app.locale') }}">
                <button
                  type="submit"
                  class="btn btn-success btn-sm"
                  title="{{ __('Gizle') }}"
                ><i class="far fa-eye"></i></button>
              @endif
            {{ Form::close() }}

            @foreach(config('ba.available_languages') as $lang)
              <a
                class="btn btn-outline-info btn-sm"
                href="{{ route('admin.portfolio.edit', [
                  $item->id,
                  'lang' => $lang,
                  ]) }}"
              >{{ __('Düzenle') }} - <strong>{{ strtoupper($lang) }}</strong></a>
            @endforeach

            {{ Form::open([
              'url' => route('admin.portfolio.destroy', $item->id),
              'method' => 'DELETE',
              'class' => 'd-inline',
              ]) }}
              <button
                type="submit"
                class="btn btn-outline-danger btn-sm"
                data-ask="{{ __('Silmek istediğinden emin misin?') }}"
              >{{ __('Sil') }}</button>
            {{ Form::close() }}
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  {{ $list->render() }}

  @if($list->count() === 0)
    <p class="text-center">
      {{ __('Hiçbir şey eklenmemiş.') }}
    </p>
  @endif
@endsection
