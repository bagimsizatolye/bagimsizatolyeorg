@extends('layouts.admin')

@section('header')
  <h3>
    {{ $portfolio->id ? $portfolio->title : __('Yeni İş Ekle') }}
    - <em>{{ strtoupper($lang) }}</em>
  </h3>
@endsection

@section('content')
  {!! Form::open([
    'url' => $portfolio->id ? route('admin.portfolio.update', $portfolio->id) : route('admin.portfolio.store'),
    'method' => $portfolio->id ? 'PUT' : 'POST',
    'files' => true,
    ]) !!}

    <input type="hidden" name="lang" value="{{ $lang }}">

    <div class="form-group">
      <label for="title">{{ __('Başlık') }}</label>
      <input
        type="text"
        name="title"
        class="form-control"
        id="title"
        maxlength="150"
        required
        value="{{ old('title') ?: $portfolio->getTranslation('title', $lang, false) }}"
      >
    </div>

    <div class="form-group">
      <label for="description">{{ __('İş tanımı') }}</label>
      <input
        type="text"
        name="description"
        class="form-control"
        id="description"
        maxlength="150"
        required
        value="{{ old('description') ?: $portfolio->getTranslation('description', $lang, false) }}"
      >
    </div>

    <div class="form-group">
      <label for="customer_name">{{ __('Müşteri') }}</label>
      <input
        type="text"
        name="customer_name"
        class="form-control"
        id="customer_name"
        maxlength="150"
        required
        value="{{ old('customer_name') ?: $portfolio->getTranslation('customer_name', $lang, false) }}"
      >
    </div>

    <div class="form-group">
      <label for="category_ids">{{ __('Kategoriler') }}</label>
      <select
        name="category_ids[]"
        class="form-control select2-init"
        id="category_ids"
        required
        multiple
      >
        @foreach($categories as $category)
          <option
            value="{{ $category->id }}"
            @if($portfolio->workCategories and $portfolio->workCategories->contains('id', $category->id)) selected @endif
          >
            {{ $category->title }}
          </option>
        @endforeach
      </select>
    </div>

    <div class="form-group">
      <label for="user_ids">{{ __('Kimler çalıştı?') }}</label>
      <select
        name="user_ids[]"
        class="form-control select2-init"
        id="user_ids"
        required
        multiple
      >
        @foreach($users as $user)
          <option
            value="{{ $user->id }}"
            @if($portfolio->users and $portfolio->users->contains('id', $user->id)) selected @endif
          >
            {{ $user->name }}
          </option>
        @endforeach
      </select>
    </div>

    <div class="form-group">
      <label for="published_at">{{ __('Yayınlanma tarihi') }}</label>
      <input
        type="date"
        name="published_at"
        class="form-control"
        id="published_at"
        value="{{ old('published_at') ?: ($portfolio->published_at ? $portfolio->published_at->format('Y-m-d') : '') }}"
      >
      <p class="help-block">
        İşin yayına girme tarihi. Tam tarih bilinmiyorsa tahmini girilebilir.
      </p>
    </div>

    <div class="form-group">
      <label for="published_at_format">{{ __('Yayın tarihi formatı') }}</label>
      <select
        name="published_at_format"
        class="form-control"
        id="published_at_format"
        value="{{ old('published_at') ?: $portfolio->published_at }}"
      >
        @foreach($publishedAtFormatList as $item)
          <option
            value="{{ $item }}"
            @if($item === (old('published_at_format') ?: $portfolio->published_at_format))
              selected
            @endif
          >{{ __($item) }}</option>
        @endforeach
      </select>
      <p class="help-block">
        İşin yayına girme tarihi. Tam tarih bilinmiyorsa tahmini girilebilir.
      </p>
    </div>

    <div class="form-group">
      <label for="link">{{ __('Bağlantı') }}</label>
      <input
        type="url"
        name="link"
        class="form-control"
        id="link"
        maxlength="1000"
        value="{{ old('link') ?: $portfolio->link }}"
      >
      <p class="help-block">
        Eğer webde bir karşılığı varsa ve aktifse buraya işin bağlantısı girilebilir.
      </p>
    </div>

    <div class="form-group">
      <label for="related_portfolio_ids">{{ __('İlgili işler') }}</label>
      <select
        name="related_portfolio_ids[]"
        class="form-control select2-init"
        id="related_portfolio_ids"
        multiple
      >
        @foreach($portfolioList as $item)
          <option
            value="{{ $item->id }}"
            @if(!empty(old('related_portfolio_ids')))
              @if(in_array($item->id, old('related_portfolio_ids')))
                selected
              @endif
            @elseif(!empty($portfolio->relatedPortfolios))
              @if($portfolio->relatedPortfolios->contains('id', $item->id))
                selected
              @endif
            @endif
          >
            {{ $item->title }} - {{ $item->workCategories->implode('title', ', ') }}
          </option>
        @endforeach
      </select>
      <p class="help-block">
        Yaptığımız diğer işlerden seçilir. Rastgele 3 tanesi gösterilecektir.
      </p>
    </div>

    <div class="form-group">
      <label for="view_type_id">{{ __('Görünüm tipi') }}</label>
      <select
        name="view_type_id"
        class="form-control"
        id="view_type_id"
      >
        @foreach(config('ba.portfolio_types') as $item)
          <option
            value="{{ $item }}"
            @if($item === (old('view_type_id') ?: $portfolio->view_type_id))
              selected
            @endif
          >{{ __('ba.portfolio_view_' . $item) }}</option>
        @endforeach
      </select>
      <p class="help-block">
        Yaptığımız diğer işlerden seçilir. Rastgele 3 tanesi gösterilecektir.
      </p>
    </div>

    <div class="form-group">
      <label for="body">{{ __('Metin') }}</label>
      <textarea
        name="body"
        class="form-control summernote-init"
        id="body"
        required
      >{!! old('body') ?: $portfolio->getTranslation('body', $lang, false) !!}</textarea>
    </div>

    <div class="form-group">
      <label for="thumbnail">{{ __('Küçük resim') }}</label>
      <input
        class="form-control"
        id="thumbnail"
        name="thumbnail"
        type="file"
        accept="image/*"
        @if(empty($portfolio->id))
          required
        @endif
      >
      <p class="help-block">
        Kare olmalı. Sadece bir tane. Listede gözükmesi için.
      </p>
    </div>

    <div class="form-group">
      <label for="media">{{ __('Görseller') }}</label>
      <input
        class="form-control"
        id="media"
        name="media[]"
        type="file"
        accept="image/*"
        multiple
      >
      <p class="help-block">
        Sınırsız ekleyebilirsin. "Kaydet"tiğinde yüklenecek. Vakit alabilir, biraz sabır.
      </p>
    </div>

    <div class="form-group">
      <label for="video">{{ __('Video') }}</label>
      <input
        class="form-control"
        id="video"
        name="video"
        type="file"
        accept="video/mp4"
        multiple
      >
      <p class="help-block">
        Sadece tek bir MP4 yükleyebilirsin. <strong>Çok</strong> vakit alabilir, biraz sabır.
      </p>
    </div>

    <div class="text-right">
      <button type="submit" class="btn btn-outline-primary">{{ __('Kaydet') }}</button>
    </div>

  {!! Form::close() !!}

  @if($portfolio->id and $portfolio->thumbnail)
    <h3>{{ __('Mevcut Küçük Resim') }}</h3>

    <img class="img-fluid" src="{{ $portfolio->thumbnail->url }}">
  @endif

  @if($portfolio->id and $portfolio->media->count())
    <h3>{{ __('Mevcut Görseller') }}</h3>

    <gallery-edit-component
      gallery-json="{!! str_replace('"', '\'', json_encode($portfolio->media->toArray(), JSON_HEX_APOS | JSON_HEX_QUOT)) !!}"
      csrf-token="{{ csrf_token() }}"
    ></gallery-edit-component>

    <!-- <div class="row">
      @foreach($portfolio->media as $media)
        <div class="col-6 col-md-3 mb-3 img-box">
          <img src="{{ $media->url_thumb }}" class="img-fluid">

          {!! Form::open([
            'url' => route('admin.media.destroy', $media->id),
            'method' => 'DELETE',
            ]) !!}
            <button
              type="submit"
              class="btn btn-danger btn-sm"
              data-ask="{{ __('Silmek istediğinden emin misin?') }}"
            ><i class="fas fa-trash"></i></button>
          {!! Form::close() !!}
        </div>
      @endforeach
    </div> -->
  @endif

@endsection
