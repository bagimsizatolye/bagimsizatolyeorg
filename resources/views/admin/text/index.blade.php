@extends('layouts.admin')

@section('header')
  <div class="row">
    <div class="col-12 col-md-8">
      <h3>bagimsizatolye.org yönetici</h3>
    </div>
  </div>
@endsection

@section('content')
  <p>
    {{ __('Başlık ve alt başlıklarda kalın yazı için <strong>kalın</strong>.') }}
    <br>
    {{ __('Alt satıra geçmek için: <br> buradan sonrası alt satır.') }}
    <br>
    {{ __('<em>Eğik yazı</em>') }}
    <br>
    {{ __('Bu kodlar sadece başlıklarda ve uzun metinlerde çalışır.') }}
  </p>

  <table class="table table-striped">
    <thead>
      <tr>
        <th>{{ __('Metin') }}</th>
        <th>
          {{ __('Türkçe') }}
          <em class="float-right">
            {{ $languageProgress['tr']['completed'] }} /
            {{ $languageProgress['tr']['total'] }}
          </em>
        </th>
        <th>
          {{ __('İngilizce') }}
          <em class="float-right">
            {{ $languageProgress['en']['completed'] }} /
            {{ $languageProgress['en']['total'] }}
          </em>
        </th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($textList as $key => $languageList)
        <tr>
          <td>
            {{ __("ba.$key") }}
          </td>
          @foreach($languageList as $languageKey => $value)
            <td>
              <form
                class="text-form"
                method="post"
                action="{{ route('admin.text.store') }}"
              >
                @csrf
                <textarea
                  name="value"
                  class="form-control"
                >{{ $value }}</textarea>
                <input type="hidden" name="lang" value="{{ $languageKey }}">
                <input type="hidden" name="key" value="{{ $key }}">
                <button type="submit" class="d-none"></button>
              </form>
            </td>
          @endforeach
        </tr>
      @endforeach
    </tbody>
  </table>
@endsection
