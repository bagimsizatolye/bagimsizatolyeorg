@extends('layouts.admin')

@section('header')
  <h3>
    {{ $user->id ? $user->name : __('Yeni Kişi Ekle') }}
    - <em>{{ strtoupper($lang) }}</em>
  </h3>
@endsection

@section('content')
  {!! Form::open([
    'url' => $user->id ? route('admin.user.update', $user->id) : route('admin.user.store'),
    'method' => $user->id ? 'PUT' : 'POST',
    'files' => true,
    ]) !!}

    <input type="hidden" name="lang" value="{{ $lang }}">

    <div class="form-group">
      <label for="name">{{ __('İsim') }}</label>
      <input
        type="text"
        name="name"
        class="form-control"
        id="name"
        maxlength="255"
        required
        value="{{ old('name') ?: $user->name }}"
        autocomplete="off"
      >
    </div>

    <div class="form-group">
      <label for="email">{{ __('E-posta') }}</label>
      <input
        type="email"
        name="email"
        class="form-control"
        id="email"
        maxlength="255"
        required
        value="{{ old('email') ?: $user->email }}"
        autocomplete="off"
      >
    </div>

    <div class="form-group">
      <label for="title">{{ __('Unvan') }}</label>
      <input
        type="text"
        name="title"
        class="form-control"
        id="title"
        maxlength="255"
        value="{{ old('title') ?: $user->getTranslation('title', $lang, false) }}"
        autocomplete="off"
      >
    </div>

    <div class="form-group">
      <label for="password">{{ __('Parola') }}</label>
      <input
        type="password"
        name="password"
        class="form-control"
        id="password"
        maxlength="32"
        value="{{ old('password') }}"
        autocomplete="new-password"
      >
    </div>

    <div class="form-group">
      <label for="password_confirmation">{{ __('Parola tekrar') }}</label>
      <input
        type="password"
        name="password_confirmation"
        class="form-control"
        id="password_confirmation"
        maxlength="32"
        value="{{ old('password_confirmation') }}"
        autocomplete="new-password"
      >
    </div>

    <div class="form-group">
      <label for="description">{{ __('Kısa geçmiş') }}</label>
      <textarea
        name="description"
        class="form-control"
        id="description"
      >{{ old('description') ?: $user->getTranslation('description', $lang, false) }}</textarea>
    </div>

    <div class="form-group">
      <label for="media">{{ __('Görsel') }}</label>
      <input
        class="form-control"
        id="media"
        name="media"
        type="file"
        accept="image/*"
      >
      <p class="help-block">
        Sadece bir tane. Kare olmalı.
      </p>
    </div>

    @if($user->photo)
      <div class="col-12 col-md-6 col-lg-4">
        <img src="{{ $user->photo->url }}" class="img-fluid" alt="{{ $user->name }}">
      </div>
    @endif

    <div class="text-right">
      <button type="submit" class="btn btn-outline-primary">{{ __('Kaydet') }}</button>
    </div>

  {!! Form::close() !!}

@endsection
