@extends('layouts.admin')

@section('header')
  <h3>bagimsizatolye.org yönetici</h3>
@endsection

@section('content')
  @if(!empty($matomoIframeURL))
    <iframe
      src="{{ $matomoIframeURL }}"
      width="100%"
      height="100%"
      class="matomo-iframe"
    ></iframe>
  @else
    Burada henüz bir şey yok.
  @endif
@endsection
