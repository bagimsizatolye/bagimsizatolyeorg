@extends('layouts.admin')

@section('header')
  <div class="row">
    <div class="col-12">
      <h3>bagimsizatolye.org yönetici</h3>
    </div>
  </div>
@endsection

@section('content')
  <table class="table table-striped table-sm table-responsive-md">
    <caption>
      {{ __('Önce cevap verilmeyenler listelenir. Bunlar da eskiden yeniye doğru sıralanır.') }}
    </caption>
    <thead class="thead-dark">
      <tr>
        <th style="min-width: 150px;">{{ __('İsim') }}</th>
        <th>{{ __('E-posta') }}</th>
        <th>{{ __('Konu') }}</th>
        <th>{{ __('Mesaj') }}</th>
        <th style="min-width: 158px;">{{ __('Cevap') }}</th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $item)
        <tr>
          <td>
            {{ $item->name }}
            <br>
            {{ format_datetime($item->created_at) }}
          </td>
          <td><a href="mailto:{{ $item->email }}">{{ $item->email }}</a></td>
          <td>{{ $item->subject }}</td>
          <td>{{ $item->text }}</td>
          <td>
            @if(!is_null($item->answered_at))
              {{ format_datetime($item->answered_at) }}<br>
              {{ $item->answeredBy->name }}
            @else
              <strong class="text-danger">{{ __('Cevap verilmedi') }}</strong>
              <br><br>
              <form method="post" action="{{ route('admin.contactform.update', $item->id) }}">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="answered_at" value="{{ now() }}">
                <button class="btn btn-sm btn-primary" type="submit">
                  {{ __('Cevap verildi olarak işaretle') }}
                </button>
              </form>
            @endif
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  {{ $list->render() }}

  @if($list->count() === 0)
    <p class="text-center">
      {{ __('Hiçbir şey eklenmemiş.') }}
    </p>
  @endif
@endsection
