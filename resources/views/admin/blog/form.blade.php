@extends('layouts.admin')

@section('header')
  <h3>{{ $blog->id ? $blog->title : __('Yeni Metin Ekle') }}</h3>
@endsection

@section('content')
  {!! Form::open([
    'url' => $blog->id ? route('admin.blog.update', $blog->id) : route('admin.blog.store'),
    'method' => $blog->id ? 'PUT' : 'POST',
    'files' => true,
    ]) !!}

    <div class="form-group">
      <label for="title">{{ __('Başlık') }}</label>
      <input
        type="text"
        name="title"
        class="form-control"
        id="title"
        maxlength="150"
        required
        value="{{ old('title') ?: $blog->title }}"
      >
    </div>

    <div class="form-group">
      <label for="body">{{ __('Metin') }}</label>
      <textarea
        name="body"
        class="form-control summernote-init"
        id="body"
        required
      >{!! old('body') ?: $blog->body !!}</textarea>
    </div>

    <div class="form-group">
      <label for="tag_ids">{{ __('Etiketler') }}</label>
      <select
        name="tag_ids[]"
        class="form-control select2-init select2-tags"
        id="tag_ids"
        required
        multiple
      >
        @foreach($tags as $tag)
          <option
            value="{{ $tag->id }}"
            @if($blog->tags and $blog->tags->contains('id', $tag->id))
              selected
            @endif
          >
            {{ $tag->title }}
          </option>
        @endforeach
      </select>
    </div>

    <div class="form-group">
      <label for="author_id">{{ __('Yazar') }}</label>
      <select
        name="author_id"
        class="form-control select2-init"
        id="author_id"
        required
      >
        @foreach($users as $user)
          <option
            value="{{ $user->id }}"
            @if($user->id == (old('author_id') ?: $blog->author_id ?: auth()->id()))
              selected
            @endif
          >
            {{ $user->name }}
          </option>
        @endforeach
      </select>
    </div>

    <div class="form-group">
      <label for="cover">{{ __('Kapak resmi') }}</label>
      <input
        class="form-control"
        id="cover"
        name="cover"
        type="file"
        accept="image/*"
        @if(empty($blog->id))
          required
        @endif
      >
      <p class="help-block">
        {{ __('Sadece bir tane. Yazının en üstünde gözükecek. Küçük resim bundan üretilecek. 16:9 oranında olmalı. 1600x900 piksel uygundur.') }}
      </p>
    </div>

    <div class="text-right">
      <button type="submit" class="btn btn-outline-primary">{{ __('Kaydet') }}</button>
    </div>

  {!! Form::close() !!}

  @if($blog->id and $blog->cover)
    <h3>{!! __('Mevcut Küçük Resim') !!}</h3>

    <img class="img-fluid" src="{{ $blog->cover->url_thumb }}">
  @endif

  @if($blog->id and $blog->cover)
    <h3>{!! __('Mevcut Kapak Resmi') !!}</h3>

    <img class="img-fluid" src="{{ $blog->cover->url }}">
  @endif

@endsection
