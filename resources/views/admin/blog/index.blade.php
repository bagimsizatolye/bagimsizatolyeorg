@extends('layouts.admin')

@section('header')
  <div class="row">
    <div class="col-12 col-md-8">
      <h3>bagimsizatolye.org yönetici</h3>
    </div>
    <div class="col-12 col-md-4 text-xs-center text-md-right">
      <a class="btn btn-outline-dark" href="{{ route('admin.blog.create') }}">
        {{ __('Yeni Metin Ekle') }}
      </a>
    </div>
  </div>
@endsection

@section('content')
  <table class="table table-striped">
    <thead>
      <tr>
        <th style="width: 60px;"></th>
        <th>{{ __('Başlık') }}</th>
        <th>{{ __('Yazar') }}</th>
        <th>{{ __('Son düzenleme') }}</th>
        <th>{{ __('Yayınlanma') }}</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $item)
        <tr>
          <td>
            @if($item->cover)
              <img src="{{ $item->cover->url_thumb }}" style="height: 30px;">
            @endif
          </td>
          <td><a href="{{ route('blog.show', $item->slug) }}">{{ $item->title }}</a></td>
          <td>
            <a href="{{ route('admin.user.edit', $item->author_id) }}">
              {{ $item->author->name }}
            </a>
          </td>
          <td>
            {{ format_datetime($item->updated_at) }}
          </td>
          <td>
            @if(!empty($item->published_at))
              {{ format_datetime($item->published_at) }}
            @else
              <em class="text-muted">{{ __('Yayınlanmadı') }}</em>
            @endif
          </td>
          <td class="text-right">
            {{ Form::open([
              'url' => route('admin.blog.update', $item->id),
              'method' => 'PUT',
              'class' => 'd-inline',
              ]) }}
              @if(empty($item->featured_at))
                <input type="hidden" name="featured_at" value="{{ now() }}">
                <button
                  type="submit"
                  class="btn btn-outline-success btn-sm"
                  title="{{ __('Öne çıkar') }}"
                ><i class="far fa-star"></i></button>
              @else
                <input type="hidden" name="featured_at" value="">
                <button
                  type="submit"
                  class="btn btn-success btn-sm"
                  title="{{ __('Öne çıkanlardan kaldır') }}"
                ><i class="fas fa-star"></i></button>
              @endif
            {{ Form::close() }}

            <a
              class="btn btn-outline-info btn-sm"
              href="{{ route('admin.blog.edit', $item->id) }}"
            >{{ __('Düzenle') }}</a>

            {{ Form::open([
              'url' => route('admin.blog.destroy', $item->id),
              'method' => 'DELETE',
              'class' => 'd-inline',
              ]) }}
              <button
                type="submit"
                class="btn btn-outline-danger btn-sm"
                data-ask="{{ __('Silmek istediğinden emin misin?') }}"
              >{{ __('Sil') }}</button>
            {{ Form::close() }}
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  @if($list->count() === 0)
    <p class="text-center">
      {{ __('Hiçbir şey eklenmemiş.') }}
    </p>
  @endif
@endsection
