@extends('layouts.admin')

@section('header')
  <div class="row">
    <div class="col-12 col-md-8">
      <h3>bagimsizatolye.org yönetici</h3>
    </div>
    <div class="col-12 col-md-4 text-xs-center text-md-right">
      <a class="btn btn-outline-dark" href="{{ route('admin.proposal.create') }}">
        {{ __('Yeni Teklif Ekle') }}
      </a>
    </div>
  </div>
@endsection

@section('content')
  <table class="table table-striped">
    <thead>
      <tr>
        <th>{{ __('Kurum') }}</th>
        <th>{{ __('Kişi') }}</th>
        <th>{{ __('Tarih') }}</th>
        <th>{{ __('Kabul') }}</th>
        <th>{{ __('Red') }}</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $item)
        <tr>
          <td>
            @if(!empty($item->concern_company_name))
              {{ $item->concern_company_name }}
            @else
              <small class="text-muted">Girilmemiş</small>
            @endif
          </td>
          <td>
            @if(!empty($item->concern_person_name))
              {{ $item->concern_person_name }}
            @else
              <small class="text-muted">Girilmemiş</small>
            @endif
          </td>
          <td>
            {{ format_date_fe($item->proposed_on) }}
          </td>
          <td>
            {{ Form::open([
              'url' => route('admin.proposal.update', $item->id),
              'method' => 'PUT',
              'class' => 'd-inline',
              ]) }}
              @if(empty($item->accepted_at))
                <input type="hidden" name="accepted_at" value="{{ now() }}">
                  <input type="hidden" name="rejected_at" value="">
                <button
                  type="submit"
                  class="btn btn-outline-success btn-sm"
                  title="{{ __('Kabul edildi olarak işaretle') }}"
                ><i class="fas fa-check"></i></button>
              @else
                <input type="hidden" name="accepted_at" value="">
                <button
                  type="submit"
                  class="btn btn-success btn-sm"
                  title="{{ __('Kabul edildi işaretini kaldır') }}"
                ><i class="fas fa-check"></i></button>
              @endif
            {{ Form::close() }}
          </td>
          <td>
            {{ Form::open([
              'url' => route('admin.proposal.update', $item->id),
              'method' => 'PUT',
              'class' => 'd-inline',
              ]) }}
              @if(empty($item->rejected_at))
                <input type="hidden" name="rejected_at" value="{{ now() }}">
                  <input type="hidden" name="accepted_at" value="">
                <button
                  type="submit"
                  class="btn btn-outline-success btn-sm"
                  title="{{ __('Reddedildi olarak işaretle') }}"
                ><i class="fas fa-times"></i></button>
              @else
                <input type="hidden" name="rejected_at" value="">
                <button
                  type="submit"
                  class="btn btn-success btn-sm"
                  title="{{ __('Reddedildi işaretini kaldır') }}"
                ><i class="fas fa-times"></i></button>
              @endif
            {{ Form::close() }}
          </td>
          <td class="text-right">
            <a
              class="btn btn-outline-primary btn-sm copy-to-clipboard"
              href="#"
              data-target="{{ route('proposal.show', ['proposalCode' => $item->code, 'pk_campaign' => 'fiyatlistesi-2020', 'pk_kwd' => str_slug($item->concern_company_name ?: $item->concern_person_name)]) }}"
            >{{ __('Kopyala') }}</a>

            <a
              class="btn btn-outline-info btn-sm"
              href="{{ route('admin.proposal.edit', $item->id) }}"
            >{{ __('Düzenle') }}</a>

            {{ Form::open([
              'url' => route('admin.proposal.destroy', $item->id),
              'method' => 'DELETE',
              'class' => 'd-inline',
              ]) }}
              <button
                type="submit"
                class="btn btn-outline-danger btn-sm"
                data-ask="{{ __('Silmek istediğinden emin misin?') }}"
              >{{ __('Sil') }}</button>
            {{ Form::close() }}
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  @if($list->count() === 0)
    <p class="text-center">
      {{ __('Hiçbir şey eklenmemiş.') }}
    </p>
  @endif
@endsection
