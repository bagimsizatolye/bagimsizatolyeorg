@extends('layouts.admin')

@section('header')
  <h3>{{ $proposal->id ? $proposal->title : __('Yeni Teklif') }}</h3>
@endsection

@section('content')
  {!! Form::open([
    'url' => $proposal->id ? route('admin.proposal.update', $proposal->id) : route('admin.proposal.store'),
    'method' => $proposal->id ? 'PUT' : 'POST',
    'files' => true,
    ]) !!}

    <div class="form-group">
      <label for="proposed_on">{{ __('Teklif tarihi') }}</label>
      <input
        type="date"
        name="proposed_on"
        class="form-control"
        id="proposed_on"
        required
        value="{{ old('proposed_on') ?: ($proposal->proposed_on ? $proposal->proposed_on->format('Y-m-d') : now()->format('Y-m-d')) }}"
      >
    </div>

    <div class="form-group">
      <label for="concern_person_name">{{ __('İlgili kişi') }}</label>
      <input
        type="text"
        name="concern_person_name"
        class="form-control"
        id="concern_person_name"
        maxlength="300"
        value="{{ old('concern_person_name') ?: $proposal->concern_person_name }}"
      >
    </div>

    <div class="form-group">
      <label for="concern_company_name">{{ __('İlgili kurum') }}</label>
      <input
        type="text"
        name="concern_company_name"
        class="form-control"
        id="concern_company_name"
        maxlength="300"
        value="{{ old('concern_company_name') ?: $proposal->concern_company_name }}"
      >
    </div>

    <div class="form-group">
      <label for="proposal_type">{{ __('Teklif tipi') }}</label>
      <select
        name="proposal_type"
        class="form-control"
        id="proposal_type"
        required
      >
        @foreach(config('ba.proposal_types') as $id => $name)
          <option
            value="{{ $id }}"
            @if($proposal and $proposal->proposal_type == $id) selected @endif
          >{{ __($name) }}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group">
      <label for="delivery_days">{{ __('İş süresi') }}</label>
      <input
        type="number"
        name="delivery_days"
        class="form-control"
        id="delivery_days"
        step="1"
        min="1"
        max="1000"
        value="{{ old('delivery_days') ?: $proposal->delivery_days }}"
      >
      <small class="helper-block">
        {{ __('İş günü olarak verilir.') }}
      </small>
    </div>

    <div class="form-group">
      <label for="bank_account">{{ __('Banka hesabı bilgileri') }}</label>
      <input
        type="text"
        name="bank_account"
        class="form-control"
        id="bank_account"
        maxlength="100"
        value="{{ old('bank_account') ?: $proposal->bank_account }}"
      >
      <small class="helper-block">
        {{ __('IBAN ve isim soyisim girilmelidir.') }}
      </small>
    </div>

    <div class="text-right">
      <button type="submit" class="btn btn-outline-primary">{{ __('Kaydet') }}</button>
    </div>

  {!! Form::close() !!}

@endsection
