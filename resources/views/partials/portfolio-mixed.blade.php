<a
  href="{{ ba_route('portfolio.show', $item->slug) }}"
  class="col-12 col-lg-4 portfolio-mixed-container"
>
  <div class="portfolio-item">
    <img src="{{ $item->thumbnail->url }}" class="img-fluid">

    <div class="info">
      <h3 class="title">{{ $item->title }}</h3>
      <h5>{{ $item->description }}</h5>
      <span class="categories">
        {{ implode(' | ', $item->workCategories->pluck('title')->toArray()) }}
      </span>
    </div>
  </div>
</a>
