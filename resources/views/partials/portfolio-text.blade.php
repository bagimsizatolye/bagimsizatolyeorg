<a
  href="{{ ba_route('portfolio.show', $item->slug) }}"
  class="col-12 col-lg-4 portfolio-item-container flip-container"
>
  <div class="portfolio-item portfolio-item-{{ $item->view_type_id }} flipper">
    <img src="{{ $item->thumbnail->url }}" class="img-fluid invisible">
    <div class="front flipper-text">
      <div>
        <div class="title">
          {{ $item->title }}
        </div>
        <div class="description">
          {{ $item->description }}
        </div>
        <div class="categories">
          {{ implode(' | ', $item->workCategories->pluck('title')->toArray()) }}
        </div>
      </div>
    </div>
    <div class="back flipper-image">
      <img src="{{ $item->thumbnail->url }}" class="img-fluid">
    </div>
  </div>
</a>
