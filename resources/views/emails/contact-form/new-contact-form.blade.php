@component('mail::message')
# Bağımsız Atölye'ye yeni iletişim formu geldi.

@component('mail::table')
|               |                             |
| ------------- |:---------------------------:|
| Gönderen      | {{ $contactForm->name }}    |
| E-posta       | {{ $contactForm->email }}   |
| Konu          | {{ $contactForm->subject }} |
| Metin         | {{ str_replace(["\r", "\n"], ' ', $contactForm->text) }}    |
@endcomponent

@component('mail::button', ['url' => route('admin.contactform.index')])
Yönetim Panelinde Gör
@endcomponent

Sevgiler, öpücükler,<br>
{{ config('app.name') }}
@endcomponent
