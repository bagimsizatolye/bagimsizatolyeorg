@extends('layouts.app')

@section('sky')
  <div class="sky-sm-bg"></div>
  <div class="fog-sm-bg"></div>

  @foreach($adminList as $admin)
    <div class="star star-{{ $loop->iteration }} star-sm"></div>
  @endforeach
@endsection

@section('content')
  <div class="container container-reading proposal-page">
    <div class="row mb-5">
      <div class="col-12 col-lg-9 text-center text-lg-left">
        @if(!empty($proposal->concern_company_name))
          <h2 class="text-uppercase font-weight-light">
            {{ $proposal->concern_company_name }}
          </h2>
        @endif
      </div>
      <div class="col-12 col-lg-3 text-center text-lg-right font-weight-bold pt-lg-3">
        {{ $proposal->proposed_on->format('d.m.Y') }}
      </div>
    </div>

    @if($proposal->concern_person_name)
      <h4 class="font-weight-bold text-center text-lg-left">
        Sayın {{ $proposal->concern_person_name }}
      </h4>
    @endif

    <p>
      Merhaba,
    </p>

    <p>
      Biz Bağımsız Atölye olarak resmi statüde olmasa da işçi kooperatifi yöntemi ile çalışmaktayız. Profesyonel olarak internet siteleri, grafik tasarım, fotoğraf, tekstil gibi alanlarda işler üretiyoruz. <a href="{{ ba_route('portfolio.index') }}" target="_blank">Buradan</a> şimdiye kadar tamamlandığımız işlere bakabilirsiniz.
    </p>

    <p class="mb-5">
      Evrensel kooperatif ilkelerinden kooperatifler arası dayanışmayı benimsiyoruz. Buna istinaden YAYKOOP paydaşı yayınevlerine özel bir fiyat listesi hazırladık. Bu listedeki fiyatlarla 2020 yılı boyunca bizden internet sitesi talep edebilirsiniz. Aşağıdan istediğiniz özellikleri belirlemeniz ve iletişime geçmeniz yeterli.
    </p>

    <h2 class="font-weight-bold mb-4">WordPress<br>İnternet Sitesi Yapım Ücreti</h2>

    <h6 class="font-weight-bold mb-3">Temel Kapsam:</h6>

    <ul class="mb-5 ba-list">
      <li>WordPress kurulumu ve optimizasyonu</li>
      <li>Tema kurulumu (Divi) ve konunuza uygun özelleştirilmesi</li>
      <li>Ana sayfa, hakkında, iletişim sayfaları yapılması</li>
      <li>Yazarlar, çevirmenler ve kitaplar sayfalarının yapılması</li>
      <li><a href="{{ request()->url() }}#mailchimpDescription">Mailchimp Entegrasyonu *</a></li>
      <li>İletişim formu – Doldurulduğunda form içeriği e-posta adresinize gelir</li>
      <li>Abaküs internet sitesi istatistikleri entegrasyonu</li>
      <li>Temel SEO düzenlemesi</li>
      <li>1 yıllık bakım ve barındırma</li>
    </ul>

    <h4 class="font-weight-bold mb-5">
      {{ __('Fiyat: :price', ['price' => config('ba.prices.basic_2020')]) }}
    </h4>

    <p id="mailchimpDescription">
      * <a href="https://mailchimp.com/" target="_blank">Mailchimp</a>, abonelerinize e-posta gönderimi yapabileceğiniz çevrimiçi bir araçtır. 2000 aboneye kadar ücretsiz kullanabilirsiniz. Bu entegrasyonu yaptığımızda internet sitenizde bir abonelik formu bulunur ve bunu dolduran kişilerin e-posta adresi, bülten listenize otomatik kayıt olur. Abonelerinize haberler vermek, kampanya duyurusu yapmak için yaygın ve etkin bir yöntemdir.
    </p>

    <hr>

    <h2 class="font-weight-bold mb-5">WordPress İnternet Sitesi Ek Özellikler</h2>

    @include('proposal.partials.ecommerce')

    <hr>

    @include('proposal.partials.maintenance')

    <hr>

    @include('proposal.partials.email_setup')

    <hr>

    @include('proposal.partials.domain')

    <hr>

    @include('proposal.partials.multilanguage')

    <hr>

    @include('proposal.partials.logo_design')

    <hr>

    @include('proposal.partials.photo_shoot')

    <hr>

    <div class="text-center mt-5">
      <a
        class="btn btn-primary"
        href="{{ ba_route('contact', ['proposal' => $proposal->id]) }}"
      >{{ __('İLETİŞİME GEÇ') }}</a>
    </div>
  </div>
@endsection
