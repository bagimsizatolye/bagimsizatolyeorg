@extends('layouts.app')

@section('sky')
  <div class="sky-sm-bg"></div>
  <div class="fog-sm-bg"></div>

  @foreach($adminList as $admin)
    <div class="star star-{{ $loop->iteration }} star-sm"></div>
  @endforeach
@endsection

@section('content')
  <div class="container container-reading proposal-page">
    <div class="row mb-5">
      <div class="col-12 col-lg-8 text-center text-lg-left">
        @if(!empty($proposal->concern_company_name))
          <h2 class="text-uppercase font-weight-light">
            {{ $proposal->concern_company_name }}
          </h2>
        @elseif(!empty($proposal->concern_person_name))
          <h2 class="text-uppercase font-weight-light">
            {{ $proposal->concern_person_name }}
          </h2>
        @endif
      </div>
      <div class="col-12 col-lg-4 text-center text-lg-right font-weight-bold pt-lg-3">
        {{ $proposal->proposed_on->format('d.m.Y') }}
      </div>
    </div>

    @if(!empty($proposal->concern_company_name) and $proposal->concern_person_name)
      <h4 class="font-weight-bold text-center text-lg-left">
        Sayın {{ $proposal->concern_person_name }}
      </h4>
    @endif

    <p class="mb-5">
      {!! strip_tags(nl2br(text('proposal_intro')), '<br>') !!}
    </p>

    <h2 class="font-weight-bold mb-4">WordPress<br>İnternet Sitesi Yapım Ücreti</h2>

    <h6 class="font-weight-bold mb-3">Temel Kapsam:</h6>

    <ul class="mb-5 ba-list">
      <li>WordPress kurulumu ve optimizasyonu</li>
      <li>Tema kurulumu (Divi) ve konunuza uygun özelleştirilmesi</li>
      <li>Ana sayfa, hizmetler (beş adet hizmet sayfası dahil), hakkında, iletişim sayfaları yapılması</li>
      <li>İletişim formu – Doldurulduğunda e-posta adresinize gelir</li>
      <li>Abaküs internet sitesi istatistikleri entegrasyonu</li>
      <li>Temel SEO düzenlemesi</li>
      <li>1 yıllık bakım ve barındırma</li>
    </ul>

    <h4 class="font-weight-bold mb-5">
      {{ __('Fiyat: :price', ['price' => config('ba.prices.basic_2020')]) }}
    </h4>

    <hr>

    <h2 class="font-weight-bold mb-5">WordPress İnternet Sitesi Ek Özellikler</h2>

    @include('proposal.partials.mailchimp')

    <hr>

    @include('proposal.partials.domain')

    <hr>

    @include('proposal.partials.multilanguage')

    <hr>

    @include('proposal.partials.content_writing')

    <hr>

    @include('proposal.partials.translation')

    <hr>

    @include('proposal.partials.logo_design')

    <hr>

    @include('proposal.partials.maintenance')

    <hr>

    @include('proposal.partials.email_setup')

    <hr>

    @include('proposal.partials.ecommerce')

    <hr>

    @include('proposal.partials.photo_shoot')

    <ul class="ba-list mt-5">
      <li>
        {{ __('Talebiniz onaylandığında ödemenin %50’si peşin, kalanı iş tesliminde yapılmalıdır.') }}
      </li>
      <li>
        {{ __('Çalışma örneği tarafınıza gönderildikten sonra en geç 5 iş günü içerisinde revize talepleri iletilmelidir. Talepleriniz 5 iş günü içerisinde uygulanacaktır.') }}
      </li>
      @if (!empty($proposal->delivery_days))
        <li>{{ __('İş teslim süresi :delivery_days iş günüdür.', ['delivery_days' => $proposal->delivery_days]) }}</li>
      @endif
      @if(!empty($proposal->bank_account))
        <li>{{ $proposal->bank_account }}</li>
      @endif
      <li>{{ __('Fiyatlara KDV ve vergilendirme dahil değildir.') }}</li>
    </ul>

    <div class="text-center mt-5 mb-5">
      <a
        class="btn btn-primary btn-lg"
        href="{{ ba_route('contact', ['proposal' => $proposal->id]) }}"
      >{{ __('İLETİŞİME GEÇ') }}</a>
    </div>
  </div>
@endsection
