<article>
  <h4 class="font-weight-bold">E-Posta Kurulumu</h4>
  <p class="mb-4">
    Kendi adres uzantınız ile Yandex üzerinden e-postalarınızı ücretsiz olarak kullanabilirsiniz. Örneğin: hayal@bagimsizatolye.org. Bu şekilde kullanabilmeniz için gerekli kurulumları sizin için yapabiliriz. Adreslerinizi herhangi bir Yandex e-postası gibi kullanabilirsiniz. Bunun için bir adet @yandex.com uzantılı e-posta adresine sahip olmalısınız (<a href="https://yandex.com.tr" target="_blank">yandex.com.tr</a> üzerinden ücretsiz).
  </p>

  <h5 class="font-weight-bold mb-3">
    {{ __('Fiyat: :price', ['price' => config('ba.prices.email_setup')]) }}
  </h5>
</article>
