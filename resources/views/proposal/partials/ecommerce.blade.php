<article class="">
  <h4 class="font-weight-bold">E-Ticaret Altyapısı ve Elektronik POS Entegrasyonu</h4>
  <p>
    WordPress çok yaygın kullanılan <a href="https://woocommerce.com/" target="_blank">WooCommerce</a> eklentisi ile hayli esnek bir e-ticaret sitesine dönüştürülebilir. İlk kez e-ticaret işine girecekler veya internet sitesi bütçesini makul düzeyde tutmak isteyenler için idealdir. Bu hizmetimiz internet sitenizin ana sayfasının bir mağaza olacak şekilde özelleştirilmesini, mağaza, kategori ve ürün sayfalarını kapsar. Ayrıca çalışacağınız bankaya yönelik elektronik POS entegrasyonu yapılır. Kargo takibi ve takip numarasının müşterilerinize e-posta ile gönderimi yapılır.
  </p>
  <p class="mb-4">
    <strong>Dikkat:</strong> Elektronik POS için öncelikle çalışacağınız bankaya başvurmalısınız.
  </p>

  <h5 class="font-weight-bold mb-3">
    {{ __('Fiyat: :price', ['price' => config('ba.prices.ecommerce')]) }}
  </h5>
</article>
