<article>
  <h4 class="font-weight-bold">Metinlerin Çevirisi ve Sitenize Girişi</h4>
  <p class="mb-4">
    İnternet sitenizi birden fazla dilde yayınlamak istiyorsanız ve bir dildeki metinleriniz hazırsa, bunları sizin için diğer dillere çevirebilir ve sitenize girebiliriz.
  </p>

  <h5 class="font-weight-bold mb-3">
    {{ __('Fiyat:') }}
    <span class="font-weight-normal">Çevrilecek dil çiftine ve kelime sayısına göre belirlenir.</span>
  </h5>
</article>
