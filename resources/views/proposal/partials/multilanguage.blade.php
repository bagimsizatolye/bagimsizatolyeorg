<article class="">
  <h4 class="font-weight-bold">Çoklu Dil Desteği</h4>
  <p>
    İnternet sitenizin birden fazla dilde olmasını istiyorsanız çoklu dil desteği almanız gerekir. Kullanıcılar istedikleri dilde sitenizi görüntüleyebilirler. Bu özelliği sağlamak için WordPress’te çok yaygın olarak kullanılan <a href="https://wpml.org/" target="_blank">WPML</a> eklentisini kullanıyoruz.
  </p>
  <p class="mb-4">
    <strong>Dikkat:</strong> Bu özelliğin kapsamı sadece çoklu dil desteğinin sağlanması ile sınırlıdır. Metinlerin yazılmasına veya çevirisine ihtiyacınız varsa iletişim formunda lütfen belirtiniz.
  </p>

  <h5 class="font-weight-bold mb-3">
    {{ __('Fiyat: :price', ['price' => config('ba.prices.multilanguage')]) }}
  </h5>
</article>
