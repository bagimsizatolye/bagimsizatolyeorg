<article>
  <h4 class="font-weight-bold">Alan Adı Alımı</h4>
  <p class="mb-4">
    Alan adı, bir internet sitesinin adresidir. Örneğin bagimsizatolye.org bir alan adıdır. Bu adrese sahip değilseniz sizin için biz alabiliriz veya mevcut sahibinden kendi hesabımıza taşıyabiliriz. Alan adları yıllık olarak ABD doları üzerinden ücretlendirilir. Eğer takibini yapmakla uğraşmak istemiyorsanız bizden alabilirsiniz.
  </p>

  <h5 class="font-weight-bold mb-3">
    {{ __('Fiyat: :price', ['price' => config('ba.prices.domain')]) }}
  </h5>
</article>
