<article>
  <h4 class="font-weight-bold">Logo Tasarımı</h4>
  <p class="mb-4">
    Logonuzun tasarımı, internet sitenizin nasıl tasarlanacağını etkileyecektir. Bu sebeple eğer logonuz yoksa öncelikle bunu yaptırmanızı öneririz. İsterseniz ihtiyacınıza uygun, sizi doğru şekilde tanıtacak bir logo tasarlayabiliriz. Bu özelliği almanız durumunda tasarlanacak logoyu tüm işlerinizde kullanabilmeniz için vektörel formattaki halini de alacaksınız.
  </p>

  <h5 class="font-weight-bold mb-3">
    {{ __('Fiyat: :price', ['price' => config('ba.prices.logo_design')]) }}
  </h5>
</article>
