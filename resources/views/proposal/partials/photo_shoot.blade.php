<article>
  <h4 class="font-weight-bold">Fotoğraf Çekimi</h4>
  <p class="mb-4">
    İnternet sitenizde asıl dikkat çekecek olan, kullanılacak görsellerdir. Bunların size özel ve profesyonel olarak hazırlanmasını istiyorsanız konunuza uygun fotoğrafları biz çekebiliriz. Bu hizmetimizi alırsanız çekilen fotoğrafların sitenize yerleştirilmesinin yanında, fotoğrafların orijinal ve yüksek boyutlu hâllerini de teslim alacaksınız. Böylelikle farklı zamanlarda dilediğinizce kullanabileceksiniz.
  </p>

  <h5 class="font-weight-bold mb-3">
    {{ __('Fiyat:') }}
    <span class="font-weight-normal">
      Senaryo, mekân ve talep edilen fotoğraf miktarına göre değişkenlik gösterir.
    </span>
  </h5>
</article>
