<article>
  <h4 class="font-weight-bold">Bakım, Yedekleme ve Barındırma</h4>
  <p class="mb-4">
    İnternet sitelerinin düzenli bakıma ihtiyacı vardır. Yazılımlar düzenli olarak güncellenmezse sitenizde birçok güvenlik açığı olur ve saldırılara karşı savunmasız kalır. Ayrıca ilk yapıldığında sorunsuz çalışan bir internet sitesi, içeriklerin artmasıyla veya teknolojinin değişmesiyle sorunlu hale gelebilir. Bu hizmetimizi aldığınızda sitenizin düzenli olarak bakımı yapılır. Bunun dışında tüm internet siteleri, çalışmak için bir sunucuya ihtiyaç duyar. Bu hizmeti aldığınızda internet siteniz WordPress için optimize edilen Bağımsız Atölye sunucularında çalışır. Bu hizmetimiz düzenli yedeklemeyi ve HTTPS ile sitenize güvenli erişimi de kapsar. <strong>Bakım, yedekleme ve barındırma ilk yıl için ücretsizdir.</strong> İnternet sitenizin güvenliği ve düzgün çalışmaya devam etmesi için sonraki yıllarda mutlaka almanızı tavsiye ederiz.
  </p>

  <h5 class="font-weight-bold mb-3">
    {{ __('Fiyat: :price', ['price' => config('ba.prices.maintenance')]) }}
  </h5>
</article>
