<article>
  <h4 class="font-weight-bold">Mailchimp Entegrasyonu</h4>
  <p class="mb-4">
    <a href="https://mailchimp.com/" target="_blank">Mailchimp</a>, abonelerinize e-posta gönderimi yapabileceğiniz online bir araçtır. 2000 aboneye kadar ücretsiz kullanabilirsiniz. Bu entegrasyonu yaptığımızda internet sitenizde bir abonelik formu bulunur ve bunu dolduran kişilerin e-posta adresi, bülten listenize otomatik kayıt olur. Abonelerinize haberler vermek, kampanya duyurusu yapmak için yaygın ve etkin bir yöntemdir.
  </p>

  <h5 class="font-weight-bold mb-3">
    {{ __('Fiyat: :price', ['price' => config('ba.prices.mailchimp')]) }}
  </h5>
</article>
