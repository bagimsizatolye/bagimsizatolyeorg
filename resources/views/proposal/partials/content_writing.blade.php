<article>
  <h4 class="font-weight-bold">İçerik (Metin) Yazılması</h4>
  <p class="mb-4">
    İnternet sitenizde bulunacak metinlerin yazılmasıdır. Eğer bunları kendiniz hazırlamak istemiyorsanız bir içerik yazarı ile çalışabilirsiniz.
  </p>

  <h5 class="font-weight-bold mb-3">
    {{ __('Fiyat: :price', ['price' => config('ba.prices.content_writing')]) }}
  </h5>
</article>
