<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $prefix = $request->route()->getPrefix();

        if (is_null($prefix)) {
            if (!App::getLocale() !== config('app.fallback_locale')) {
                set_locale(config('app.fallback_locale'));
            }
        } else {
            $prefix = str_replace('/', '', $prefix);

            if (in_array($prefix, config('ba.available_languages'))) {
                set_locale($prefix);
            }
        }

        return $next($request);
    }
}
