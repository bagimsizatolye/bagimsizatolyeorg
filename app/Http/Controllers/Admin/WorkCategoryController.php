<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use App\Http\Controllers\Controller;
use App\Models\WorkCategory;
use App\Services\PortfolioService;

class WorkCategoryController extends Controller
{
    /**
     * @var PortfolioService
     */
    protected $portfolioService;

    public function __construct(
        PortfolioService $portfolio
    ) {
        $this->portfolioService = $portfolio;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
        $list = $this->portfolioService->getCategoryAdminList();

        return view('admin.work-category.index', [
            'list' => $list,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(): View
    {
        return view('admin.work-category.form', [
            'workCategory' => new WorkCategory(),
            'lang' => config('app.fallback_locale'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): RedirectResponse
    {
        $postData = $request->validate([
            'title' => 'required',
            'body' => 'required',
            'icon' => 'required',
        ]);

        $workCategory = $this->portfolioService->saveCategory(
            new WorkCategory(),
            config('app.fallback_locale'),
            $postData
        );

        return redirect()->to(route('admin.work-category.edit', $workCategory->id))
            ->with('success', __('Kaydedildi'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WorkCategory  $workCategory
     * @return \Illuminate\Http\Response
     */
    public function show(WorkCategory $workCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WorkCategory  $workCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(
        Request $request,
        WorkCategory $workCategory
    ): View {
        $lang = $request->input('lang', config('app.fallback_locale'));

        return view('admin.work-category.form', [
            'workCategory' => $workCategory,
            'lang' => $lang,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WorkCategory  $workCategory
     * @return \Illuminate\Http\Response
     */
    public function update(
        Request $request,
        WorkCategory $workCategory
    ): RedirectResponse {
        $postData = $request->validate([
            'title' => 'required',
            'body' => 'required',
            'icon' => '',
            'lang' => 'required|in:' .
                implode(',', config('ba.available_languages')),
        ]);

        $this->portfolioService->saveCategory(
            $workCategory,
            $postData['lang'],
            $postData
        );

        return redirect()->back()
            ->with('success', __('Kaydedildi'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WorkCategory  $workCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkCategory $workCategory): RedirectResponse
    {
        $workCategory->delete();

        return redirect()->back()
            ->with('success', __('Silindi'));
    }
}
