<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Tag;
use App\Services\BlogService;
use App\Services\TagService;
use App\Services\UserService;

class BlogController extends Controller
{
    /**
     * @var BlogService
     */
    protected $blogService;

    /**
     * @var TagService
     */
    protected $tagService;

    /**
     * @var UserService
     */
    protected $userService;

    public function __construct(
        BlogService $blogService,
        TagService $tagService,
        UserService $userService
    ) {
        $this->blogService = $blogService;
        $this->tagService = $tagService;
        $this->userService = $userService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
        $list = $this->blogService->getAdminList();

        return view('admin.blog.index', [
            'list' => $list,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        $tags = $this->tagService->getAdminList();
        $users = $this->userService->getAdminList();

        return view('admin.blog.form', [
            'blog' => new Blog(),
            'users' => $users,
            'tags' => $tags,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = $request->validate([
            'title' => 'required',
            'body' => 'required',
            'author_id' => 'required',
            'tag_ids' => 'required|array',
            'cover' => 'required',
        ]);

        $postData['published_at'] = now();

        if (isset($postData['tag_ids']) and is_array($postData['tag_ids'])) {
            foreach ($postData['tag_ids'] as $key => $tagId) {
                if ((int)$tagId === 0 and is_string($tagId)) {
                    $tag = $this->tagService->save(new Tag(), [
                        'title' => $tagId,
                    ]);
                    $postData['tag_ids'][$key] = $tag->id;
                }
            }
        }

        $blog = $this->blogService->save(new Blog(), $postData);

        return redirect()->to(route('admin.blog.edit', $blog->id))
            ->with('success', __('Kaydedildi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Blog  $blog
     * @return \Illuminate\View\View
     */
    public function edit(Blog $blog): View
    {
        $blog->load([
            'cover',
        ]);

        $tags = $this->tagService->getAdminList();
        $users = $this->userService->getAdminList();

        return view('admin.blog.form', [
            'blog' => $blog,
            'users' => $users,
            'tags' => $tags,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        $postData = $request->validate([
            'title' => 'required|sometimes',
            'body' => 'required|sometimes',
            'author_id' => 'required|sometimes',
            'tag_ids' => 'required|array|sometimes',
            'cover' => 'required|sometimes',
            'featured_at' => 'sometimes',
        ]);

        $postData['published_at'] = now();

        if (isset($postData['tag_ids']) and is_array($postData['tag_ids'])) {
            foreach ($postData['tag_ids'] as $key => $tagId) {
                if ((int)$tagId === 0 and is_string($tagId)) {
                    $tag = $this->tagService->save(new Tag(), [
                        'title' => $tagId,
                    ]);
                    $postData['tag_ids'][$key] = $tag->id;
                }
            }
        }

        $blog = $this->blogService->save($blog, $postData);

        return redirect()->back()
            ->with('success', __('Kaydedildi'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        $blog->delete();

        return redirect()->back()
            ->with('success', __('Silindi'));
    }
}
