<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

use App\Http\Controllers\Controller;
use App\Models\Portfolio;
use App\Services\PortfolioService;
use App\Services\UserService;

class PortfolioController extends Controller
{
    /**
     * @var PortfolioService
     */
    protected $portfolioService;

    /**
     * @var UserService
     */
    protected $userService;

    public function __construct(
        PortfolioService $portfolio,
        UserService $userService
    ) {
        $this->portfolioService = $portfolio;
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
        $list = $this->portfolioService->getAdminList(30);

        return view('admin.portfolio.index', [
            'list' => $list,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = $this->userService->getAdminList();
        $categories = $this->portfolioService->getCategoryAdminList();
        $portfolioList = $this->portfolioService->adminAllExcept(new Portfolio());
        $publishedAtFormatList = Portfolio::PUBLISHED_AT_FORMATS;

        return view('admin.portfolio.form', [
            'portfolio' => new Portfolio(),
            'portfolioList' => $portfolioList,
            'publishedAtFormatList' => $publishedAtFormatList,
            'users' => $users,
            'categories' => $categories,
            'lang' => config('app.fallback_locale'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = $request->validate([
            'title' => 'required',
            'body' => 'required',
            'customer_name' => 'required',
            'category_ids' => 'required|array',
            'description' => 'required',
            'user_ids' => 'required|array',
            'media' => 'required|array',
            'media.*' => 'image|mimes:jpeg,png',
            'published_at' => '',
            'published_at_format' => '',
            'thumbnail' => 'required|mimes:jpeg,png',
            'featured_at' => '',
            'link' => '',
            'related_portfolio_ids' => 'array',
            'video' => 'mimes:mp4',
            'view_type_id' => 'required',
        ]);

        $portfolio = $this->portfolioService->save(
            new Portfolio(),
            config('app.fallback_locale'),
            $postData
        );

        return redirect()->to(route('admin.portfolio.edit', $portfolio->id))
            ->with('success', __('Kaydedildi'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function show(Portfolio $portfolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Portfolio $portfolio): View
    {
        $lang = $request->input('lang', config('app.fallback_locale'));
        $users = $this->userService->getAdminList();
        $categories = $this->portfolioService->getCategoryAdminList();
        $portfolioList = $this->portfolioService->adminAllExcept($portfolio);
        $publishedAtFormatList = Portfolio::PUBLISHED_AT_FORMATS;

        return view('admin.portfolio.form', [
            'portfolio' => $portfolio,
            'portfolioList' => $portfolioList,
            'publishedAtFormatList' => $publishedAtFormatList,
            'users' => $users,
            'categories' => $categories,
            'lang' => $lang,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Portfolio $portfolio)
    {
        $postData = $request->validate([
            'title' => 'required|sometimes',
            'body' => 'required|sometimes',
            'customer_name' => 'required|sometimes',
            'category_ids' => 'required|array|sometimes',
            'description' => 'required|sometimes',
            'is_visible' => 'required|boolean|sometimes',
            'user_ids' => 'required|array|sometimes',
            'lang' => 'required|in:' .
                implode(',', config('ba.available_languages')),
            'media' => 'sometimes|array',
            'media.*' => 'sometimes|image|mimes:jpeg,png',
            'published_at' => 'sometimes',
            'published_at_format' => 'sometimes',
            'thumbnail' => 'sometimes|image|mimes:jpeg,png',
            'featured_at' => 'sometimes',
            'link' => 'sometimes',
            'related_portfolio_ids' => 'array|sometimes',
            'video' => 'sometimes|mimes:mp4',
            'view_type_id' => 'sometimes|required',
        ]);

        $this->portfolioService->save($portfolio, $postData['lang'], $postData);

        return redirect()->back()
            ->with('success', __('Kaydedildi'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Portfolio $portfolio)
    {
        $portfolio->delete();

        return redirect()->back()
            ->with('success', __('Silindi'));
    }
}
