<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\View\View;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\UserService;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    protected $userService;

    public function __construct(
        UserService $userService
    ) {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
        $list = $this->userService->getAdminList();

        return view('admin.user.index', [
            'list' => $list,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(): View
    {
        return view('admin.user.form', [
            'user' => new User(),
            'lang' => config('app.fallback_locale'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): RedirectResponse
    {
        $postData = $request->validate([
            'title' => '',
            'email' => [
                'required',
                'email',
                Rule::unique('users'),
            ],
            'name' => 'required',
            'description' => '',
            'password' => 'nullable|min:6|confirmed',
            'password_confirmation' => 'nullable|min:6|required_with:password',
            'media' => 'required|image|mimes:jpeg,png',
        ]);

        $user = $this->userService->save(
            new User(),
            config('app.fallback_locale'),
            $postData
        );

        return redirect()->to(route('admin.user.edit', $user->id))
            ->with('success', __('Kaydedildi'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, User $user): View
    {
        $lang = $request->input('lang', config('app.fallback_locale'));

        return view('admin.user.form', [
            'user' => $user,
            'lang' => $lang,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user): RedirectResponse
    {
        $postData = $request->validate([
            'title' => '',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id),
            ],
            'name' => 'required',
            'description' => '',
            'password' => 'nullable|min:6|confirmed',
            'password_confirmation' => 'nullable|min:6|required_with:password',
            'media' => 'image|mimes:jpeg,png',
            'lang' => 'required|in:' .
                implode(',', config('ba.available_languages')),
        ]);

        if (empty($postData['password'])) {
            unset($postData['password']);
        }

        $this->userService->save($user, $postData['lang'], $postData);

        return redirect()->back()
            ->with('success', __('Kaydedildi'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user): RedirectResponse
    {
        $user->delete();

        return redirect()->back()
            ->with('success', __('Silindi'));
    }
}
