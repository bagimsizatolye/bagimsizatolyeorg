<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Text;
use App\Services\TextService;

class TextController extends Controller
{
    /**
     * @var TextService
     */
    protected $textService;

    public function __construct(TextService $textService)
    {
        $this->textService = $textService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $textList = $this->textService->getAdminList();
        $languageProgress = [];

        foreach ($textList as $key => $languageList) {
            if (strpos($key, 'seo_') !== false) {
                continue;
            }

            foreach ($languageList as $langKey => $value) {
                if (!isset($languageProgress[$langKey])) {
                    $languageProgress[$langKey] = [
                        'total' => 0,
                        'completed' => 0,
                        'empty' => 0,
                    ];
                }

                if (empty($value)) {
                    $languageProgress[$langKey]['empty']++;
                } else {
                    $languageProgress[$langKey]['completed']++;
                }

                $languageProgress[$langKey]['total']++;
            }
        }

        return view('admin.text.index', [
            'textList' => $textList,
            'languageProgress' => $languageProgress,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->validate([
            'value' => 'required|string|nullable',
            'key' => 'required|string',
            'lang' => 'in:tr,en',
        ]);

        $text = $this->textService->getText($formData['key'], $formData['lang']);

        if (!$text) {
            $text = $this->textService->saveText(
                new Text(),
                $formData['key'],
                $formData['lang'],
                $formData['value']
            );
        } else {
            $text = $this->textService->saveText(
                $text,
                $formData['key'],
                $formData['lang'],
                $formData['value']
            );
        }

        return response()->json($text);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
