<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Proposal;
use App\Services\ProposalService;

class ProposalController extends Controller
{
    /**
     * @var ProposalService
     */
    protected $proposalService;

    public function __construct(
        ProposalService $proposalService
    ) {
        $this->proposalService = $proposalService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->proposalService->getAdminList();

        return view('admin.proposal.index', [
            'list' => $list,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.proposal.form', [
            'proposal' => new Proposal(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postData = $request->validate([
            'concern_company_name' => 'nullable',
            'concern_person_name' => 'nullable',
            'proposal_type' => 'required',
            'proposed_on' => 'required',
            'delivery_days' => 'int|min:1',
            'bank_account' => '',
        ]);

        $proposal = $this->proposalService->save(new Proposal(), $postData);

        return redirect()->to(route('admin.proposal.edit', $proposal->id))
            ->with('success', __('Kaydedildi'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function show(Proposal $proposal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function edit(Proposal $proposal)
    {
        return view('admin.proposal.form', [
            'proposal' => $proposal,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proposal $proposal)
    {
        $postData = $request->validate([
            'concern_company_name' => 'sometimes',
            'concern_person_name' => 'sometimes',
            'accepted_at' => 'sometimes',
            'rejected_at' => 'sometimes',
            'proposal_type' => 'sometimes|required',
            'proposed_on' => 'sometimes|required',
            'delivery_days' => 'sometimes|int|min:1',
            'bank_account' => 'sometimes',
        ]);

        $proposal = $this->proposalService->save($proposal, $postData);

        return redirect()->to(route('admin.proposal.index'))
            ->with('success', __('Kaydedildi'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proposal $proposal)
    {
        $proposal->delete();

        return redirect()->back()
            ->with('success', __('Silindi'));
    }
}
