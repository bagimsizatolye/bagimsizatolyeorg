<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;

use App\Models\Proposal;
use App\Services\ProposalService;
use App\Support\MetaData;

class ProposalController extends Controller
{
    /**
     * @var ProposalService
     */
    protected $proposalService;

    public function __construct(
        ProposalService $proposalService
    ) {
        $this->proposalService = $proposalService;
    }

    public function show(string $proposalCode): View
    {
        $proposal = $this->proposalService->getByCode($proposalCode);

        if (empty($proposal)) {
            abort(404);
        }

        $meta = new MetaData([
            'title' => text('seo_title'),
        ]);

        return view($proposal->proposal_type, [
            'meta' => $meta,
            'proposal' => $proposal,
        ]);
    }
}
