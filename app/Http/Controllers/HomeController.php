<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;

use App\Models\ContactForm;
use App\Services\BlogService;
use App\Services\PortfolioService;
use App\Services\ContactFormService;
use App\Services\UserService;
use App\Services\TagService;
use App\Services\TextService;
use App\Support\MetaData;

class HomeController extends Controller
{
    /**
     * @var PortfolioService
     */
    public $portfolioService;

    /**
     * @var ContactFormService
     */
    public $contactFormService;

    /**
     * @var UserService
     */
    public $userService;

    /**
     * @var BlogService
     */
    public $blogService;

    /**
     * @var TagService
     */
    public $tagService;

    /**
     * @var TextService
     */
    public $textService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        PortfolioService $portfolio,
        ContactFormService $contactFormService,
        BlogService $blogService,
        TagService $tagService,
        TextService $textService,
        UserService $userService
    ) {
        $this->portfolioService = $portfolio;
        $this->contactFormService = $contactFormService;
        $this->userService = $userService;
        $this->blogService = $blogService;
        $this->tagService = $tagService;
        $this->textService = $textService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(): View
    {
        $list = $this->portfolioService->getListFeatured();
        $workCategories = $this->portfolioService->getCategoryList();

        $meta = new MetaData([
            'title' => text('seo_title'),
        ]);

        return view('home', [
            'list' => $list,
            'meta' => $meta,
            'workCategories' => $workCategories,
        ]);
    }

    public function services(): View
    {
        $users = $this->userService->getList();
        $list = $this->portfolioService->getListFeatured();
        $workCategories = $this->portfolioService->getCategoryList();

        $meta = new MetaData([
            'title' => text('seo_services_title'),
            'description' => text('seo_services_description'),
            'keywords' => text('seo_services_keywords'),
        ]);

        return view('pages.services', [
            'list' => $list,
            'meta' => $meta,
            'workCategories' => $workCategories,
            'users' => $users,
        ]);
    }

    public function manifest(): View
    {
        $meta = new MetaData([
            'title' => text('seo_manifest_title'),
            'description' => text('seo_manifest_description'),
            'keywords' => text('seo_manifest_keywords'),
        ]);

        return view('pages.manifest-' . app()->getLocale(), [
            'meta' => $meta,
        ]);
    }

    public function portfolio(): View
    {
        $list = $this->portfolioService->getList();
        $workCategories = $this->portfolioService->getActiveCategoryList();

        $meta = new MetaData([
            'title' => text('seo_portfolio_title'),
            'description' => text('seo_portfolio_description'),
            'keywords' => text('seo_portfolio_keywords'),
        ]);

        return view('pages.portfolio-index', [
            'meta' => $meta,
            'list' => $list,
            'workCategories' => $workCategories,
        ]);
    }

    public function portfolioShow(string $slug): View
    {
        /**
         * @var \App\Models\Portfolio
         */
        $portfolio = $this->portfolioService->getBySlug($slug);

        if (!$portfolio) {
            abort(404);
        }

        if ($portfolio->slug !== $slug) {
            $translations = $portfolio->getTranslations('slug');
            set_locale(array_search($slug, $translations));
        }

        $portfolio->load([
            'media',
            'workCategories',
            'users',
            'relatedPortfolios',
            'relatedPortfolios.workCategories',
            'relatedPortfolios.thumbnail',
        ]);

        $meta = new MetaData([
            'title' => $portfolio->title,
            'description' => $portfolio->workCategories->implode('title', ', ') .
                '. ' . $portfolio->description . '. ' .
                str_limit(strip_tags($portfolio->body), 250),
            'keywords' => $portfolio->workCategories->implode('title', ', ') .
                ', ' . $portfolio->description . ', ' . $portfolio->customer_name,
            'image' => $portfolio->media->first()->url,
            'imageWidth' => $portfolio->media->first()->width,
            'imageHeight' => $portfolio->media->first()->height,
        ]);

        return view('pages.portfolio-show', [
            'meta' => $meta,
            'portfolio' => $portfolio,
        ]);
    }

    public function contact(Request $request)
    {
        if ($request->isMethod('POST')) {
            $formData = $request->validate([
                'name' => 'required',
                'email' => 'required|email',
                'subject' => 'required',
                'text' => 'required',
                'email_check' => '',
            ]);

            if (!empty($formData['email_check'])) {
                return redirect()->back()
                    ->with('errors', collect(__('Form gönderilemedi. Tekrar dener misiniz?')))
                    ->withInput();
            }

            $model = $this->contactFormService->save(new ContactForm(), $formData);

            if ($model->id) {
                return redirect()->back()
                    ->with('success', __('Bilgilerinizi aldık. En yakın zamanda size ulaşacağız.'));
            }

            return redirect()->back()
                ->with('errors', collect(__('Form gönderilemedi. Tekrar dener misiniz?')))
                ->withInput();
        }

        $meta = new MetaData([
            'title' => text('seo_contact_title'),
            'description' => text('seo_contact_description'),
            'keywords' => text('seo_contact_keywords'),
        ]);

        return view('pages.contact', [
            'meta' => $meta,
        ]);
    }

    public function blogIndex(): View
    {
        $blogList = $this->blogService->getLiveList();

        $meta = new MetaData([
            'title' => text('seo_blog_title'),
            'description' => text('seo_blog_description'),
            'keywords' => text('seo_blog_keywords'),
        ]);

        return view('pages.blog-index', [
            'blogList' => $blogList,
            'meta' => $meta,
        ]);
    }

    public function blogShow(string $slug): View
    {
        $blog = $this->blogService->getBySlug($slug);

        if (!$blog) {
            abort(404);
        }

        $this->blogService->loadDetailRelations($blog);
        $latestBlogList = $this->blogService->getLiveList(3, [$blog->id]);

        $meta = new MetaData([
            'title' => $blog->title,
            'description' => $blog->tags->implode('title', ', ') .
                '. ' . str_limit(strip_tags($blog->body), 250),
            'keywords' => $blog->tags->implode('title', ', ') .
                ', ' . $blog->author->name,
            'image' => $blog->cover->url,
            'imageWidth' => $blog->cover->width,
            'imageHeight' => $blog->cover->height,
        ]);

        return view('pages.blog-show', [
            'blog' => $blog,
            'latestBlogList' => $latestBlogList,
            'meta' => $meta,
        ]);
    }

    public function tagShow(string $slug): View
    {
        $tag = $this->tagService->getBySlug($slug);

        if (!$tag) {
            abort(404);
        }

        $blogList = $this->blogService->getLiveListForTag($tag);

        if ($blogList->count() === 0) {
            abort(404);
        }

        $meta = new MetaData([
            'title' => $tag->title,
            'image' => $blogList->first()->cover->url,
            'imageWidth' => $blogList->first()->cover->width,
            'imageHeight' => $blogList->first()->cover->height,
        ]);

        return view('pages.blog-index', [
            'blogList' => $blogList,
            'meta' => $meta,
            'tag' => $tag,
        ]);
    }
}
