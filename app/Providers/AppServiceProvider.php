<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

use App\Services\BlogService;
use App\Services\UserService;
use App\Support\MetaData;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(
        Request $request,
        BlogService $blogService,
        UserService $userService
    ) {
        $blogList = $blogService->getLiveList(3);
        View::share('latestBlogList', $blogList);

        $adminList = $userService->getAdminList();
        View::share('adminList', $adminList);

        view()->composer('layouts.app', function ($view) {
            if (empty($view->meta)) {
                $meta = new MetaData();
                $view->with(compact('meta'));
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
