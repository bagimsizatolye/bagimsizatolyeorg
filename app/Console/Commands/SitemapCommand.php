<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

use App\Services\BlogService;
use App\Services\PortfolioService;
use App\Services\TagService;

class SitemapCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ba:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates the sitemap.xml file for search engines';

    /**
     * Sitemap object
     * @var Sitemap
     */
    protected $sitemap;

    /**
     * BlogService
     * @var BlogService
     */
    protected $blogService;

    /**
     * PortfolioService
     * @var PortfolioService
     */
    protected $portfolioService;

    /**
     * TagService
     * @var TagService
     */
    protected $tagService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        BlogService $blogService,
        PortfolioService $portfolioService,
        TagService $tagService
    ) {
        parent::__construct();

        $this->blogService = $blogService;
        $this->portfolioService = $portfolioService;
        $this->tagService = $tagService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->sitemap = Sitemap::create();

        foreach (config('ba.available_languages') as $locale) {
            set_locale($locale);
            $this->generateSitemapForLocale($locale);
        }

        $this->sitemap->writeToFile('public/sitemap.xml');
    }

    public function generateSitemapForLocale(string $locale)
    {
        $now = Carbon::now();

        $homeLastChange = Carbon::create(\exec('git log -1 --date=iso --format=%cd resources/views/home.blade.php'));
        $portfolioFeaturedUpdatedAt = $this->portfolioService->getListFeatured()
            ->sortByDesc('updated_at')
            ->first()->updated_at;

        if ($portfolioFeaturedUpdatedAt > $homeLastChange) {
            $homeLastChange = $portfolioFeaturedUpdatedAt;
        }

        $this->sitemap->add(Url::create(ba_route('home', [], $locale))
            ->setPriority(1)
            ->setChangeFrequency('daily')
            ->setLastModificationDate($homeLastChange));

        $manifestLastChange = Carbon::create(\exec('git log -1 --date=iso --format=%cd resources/views/pages/manifest-' . $locale . '.blade.php'));

        $this->sitemap->add(Url::create(ba_route('manifest', [], $locale))
            ->setPriority(0.64)
            ->setChangeFrequency('monthly')
            ->setLastModificationDate($manifestLastChange));

        $servicesLastChange = Carbon::create(\exec('git log -1 --date=iso --format=%cd resources/views/pages/services.blade.php'));

        $this->sitemap->add(Url::create(ba_route('services', [], $locale))
            ->setPriority(0.8)
            ->setChangeFrequency('monthly')
            ->setLastModificationDate($servicesLastChange));

        $contactLastChange = Carbon::create(\exec('git log -1 --date=iso --format=%cd resources/views/pages/contact.blade.php'));

        $this->sitemap->add(Url::create(ba_route('contact', [], $locale))
            ->setPriority(0.8)
            ->setChangeFrequency('monthly')
            ->setLastModificationDate($contactLastChange));

        $this->addPortfolios($locale)
            ->addBlogs($locale);
    }

    protected function addPortfolios(string $locale)
    {
        $list = $this->portfolioService->getList();

        $indexLastChanged = $list->sortByDesc('updated_at')
            ->first()->updated_at;

        $this->sitemap->add(
            Url::create(ba_route('portfolio.index'), [], $locale)
                ->setPriority(0.8)
                ->setChangeFrequency('daily')
                ->setLastModificationDate($indexLastChanged)
        );

        foreach ($list as $item) {
            $this->sitemap->add(
                Url::create(ba_route('portfolio.show', $item->slug, $locale))
                    ->setPriority(0.64)
                    ->setChangeFrequency('weekly')
                    ->setLastModificationDate($item->updated_at)
            );
        }

        return $this;
    }

    protected function addBlogs(string $locale)
    {
        $blogList = $this->blogService->getLiveList();

        $blogIndexLastChanged = $blogList->sortByDesc('updated_at')
            ->first()->updated_at;

        $this->sitemap->add(
            Url::create(ba_route('blog.index', [], $locale))
                ->setPriority(0.6)
                ->setChangeFrequency('daily')
                ->setLastModificationDate($blogIndexLastChanged)
        );

        foreach ($blogList as $blog) {
            $this->sitemap->add(
                Url::create(ba_route('blog.show', $blog->slug, $locale))
                    ->setPriority(0.5)
                    ->setChangeFrequency('weekly')
                    ->setLastModificationDate($blog->updated_at)
            );
        }

        $tagList = $this->tagService->getLiveList();

        foreach ($tagList as $item) {
            $this->sitemap->add(
                Url::create(ba_route('blog.tag.show', $item->slug, $locale))
                    ->setPriority(0.3)
                    ->setChangeFrequency('daily')
                    ->setLastModificationDate($item->blogs()->orderBy('published_at')->first()->updated_at)
            );
        }

        return $this;
    }
}
