<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\User;

class CreateAdminUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ba:create-admin-user {--name=} {--email=} {--password=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates an admin user and prints its password';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->option('name');
        $email = $this->option('email');
        $password = $this->option('password');

        if (empty($name)) {
            $this->error('Missing name input');
            return;
        }

        if (empty($email)) {
            $this->error('Missing email input');
            return;
        }

        if (empty($password)) {
            $this->error('Missing password input');
            return;
        }

        $user = User::create([
            'email' => $email,
            'name' => $name,
            'email_verified_at' => \Carbon\Carbon::now(),
            'password' => bcrypt($password),
        ]);

        $this->info('User created: ' . $email);
    }
}
