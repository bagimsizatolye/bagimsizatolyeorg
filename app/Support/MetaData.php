<?php

namespace App\Support;

class MetaData
{
    public $seo;

    public function __construct(array $metaData = [])
    {
        $this->seo = new \stdClass();
        $this->seo->abstract = '';
        $this->seo->description = text('seo_description');
        $this->seo->keywords = text('seo_keywords');
        $this->seo->title = text('seo_title');
        $this->seo->image = asset('img/logo-dark-white-bg.png');
        $this->seo->newsKeywords = '';
        $this->seo->title = config('app.name');
        $this->seo->url = url()->current();

        foreach ($metaData as $key => $value) {
            if (empty($value)) {
                continue;
            }
            $this->seo->$key = $value;
        }

        if ($this->seo->title !== text('seo_title')) {
            $this->seo->title = $this->seo->title .  ' | ' . text('seo_title');
        }

        if (\strpos($this->seo->image, 'http') === false) {
            $this->seo->image = url($this->seo->image);
        }

        if ($this->seo->image === asset('img/logo-dark-white-bg.png')) {
            $this->seo->imageWidth = 1080;
            $this->seo->imageHeight = 512;
        }
    }
}
