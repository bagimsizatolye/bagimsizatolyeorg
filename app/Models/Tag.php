<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Tag
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Blog[] $blogs
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag query()
 * @mixin \Eloquent
 */
class Tag extends Model
{
    protected $fillable = [
        'title',
        'slug',
    ];

    public $timestamps = false;

    public function blogs()
    {
        return $this->belongsToMany(
            Blog::class,
            'blog_tags',
            'tag_id',
            'blog_id'
        );
    }
}
