<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    protected $fillable = [
        'concern_company_name',
        'concern_person_name',
        'accepted_at',
        'rejected_at',
        'proposal_type',
        'proposed_on',
    ];

    public $casts = [
        'accepted_at' => 'date',
        'rejected_at' => 'date',
        'proposed_on' => 'date',
    ];
}
