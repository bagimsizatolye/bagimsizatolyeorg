<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactForm extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'email',
        'subject',
        'text',
        'answered_by_id',
        'answered_at',
    ];

    public $dates = [
        'created_at',
        'updated_at',
        'answered_at',
    ];

    public function answeredBy()
    {
        return $this->belongsTo(User::class, 'answered_by_id');
    }
}
