<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasTranslations;

class WorkCategory extends Model
{
    use HasTranslations;

    public $fillable = [
        'title',
        'body',
        'slug',
    ];

    public $translatable = [
        'title',
        'body',
        'slug',
    ];

    public function icon()
    {
        return $this->hasOne(Media::class, 'related_id')
            ->relatedTo('work-category-icon');
    }
}
