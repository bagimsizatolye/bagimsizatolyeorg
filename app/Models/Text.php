<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Authentik\EloquentCache\Cacheable;

class Text extends Model
{
    use Cacheable;

    protected $fillable = [
        'key',
        'value',
        'lang',
    ];

    public function getCacheTTL()
    {
        return 60 * 24;
    }
}
