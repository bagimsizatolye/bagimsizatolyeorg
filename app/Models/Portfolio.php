<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\HasTranslations;

class Portfolio extends Model
{
    use SoftDeletes,
        HasTranslations;

    const PUBLISHED_AT_FORMATS = [
        'YYYY',
        'MMMM YYYY',
        'D MMMM YYYY',
    ];

    public $fillable = [
        'title',
        'slug',
        'body',
        'published_at',
        'published_at_format',
        'view_type_id',
        'description',
        'featured_at',
        'customer_name',
        'link',
        'is_visible',
    ];

    public $dates = [
        'featured_at',
        'published_at',
    ];

    public $appends = [
        'url',
    ];

    public $translatable = [
        'title',
        'slug',
        'body',
        'description',
        'customer_name',
    ];

    public $casts = [
        'is_visible' => 'boolean',
    ];

    public function media()
    {
        return $this->hasMany(Media::class, 'related_id')
            ->relatedTo('portfolio')
            ->orderBy('order_num');
    }

    public function video()
    {
        return $this->hasOne(Media::class, 'related_id')
            ->relatedTo('portfolio-video')
            ->type('video')
            ->orderBy('order_num');
    }

    public function thumbnail()
    {
        return $this->hasOne(Media::class, 'related_id')
            ->relatedTo('portfolio-thumbnail');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'porfolio_users', 'portfolio_id', 'user_id');
    }

    public function workCategories()
    {
        return $this->belongsToMany(
            WorkCategory::class,
            'porfolio_categories',
            'portfolio_id',
            'work_category_id'
        );
    }

    public function relatedPortfolios()
    {
        return $this->belongsToMany(
            Portfolio::class,
            'portfolio_relations',
            'portfolio_id',
            'related_portfolio_id'
        )->orderBy('order_num', 'asc');
    }

    public function getUrlAttribute(): string
    {
        return ba_route('portfolio.show', $this->slug);
    }
}
