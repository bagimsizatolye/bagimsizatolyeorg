<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\ContactForm;

class NewContactForm extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var ContactForm
     */
    public $contactForm;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContactForm $contactForm)
    {
        $this->contactForm = $contactForm;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@bird.bagimsizatolye.org')
            ->replyTo($this->contactForm->email, $this->contactForm->name)
            ->subject('[BağımsızAtölye] ' . __('Yeni iletişim formu geldi'))
            ->markdown('emails.contact-form.new-contact-form');
    }
}
