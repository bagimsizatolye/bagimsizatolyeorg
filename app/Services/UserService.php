<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\Media;
use App\Models\User;
use App\Services\MediaService;

class UserService
{
    /**
     * @var MediaService
     */
    protected $mediaService;

    public function __construct(MediaService $media)
    {
        $this->mediaService = $media;
    }

    public function getAdminList(): Collection
    {
        return User::all();
    }

    public function getList(): Collection
    {
        return User::with([
            'photo',
        ])->get();
    }

    public function save(User $model, string $lang, array $data): User
    {
        if (array_key_exists('name', $data)) {
            $model->name = $data['name'];
        }

        if (array_key_exists('title', $data)) {
            $model->setTranslation('title', $lang, $data['title']);
        }

        if (array_key_exists('description', $data)) {
            $model->setTranslation('description', $lang, $data['description']);
        }

        if (array_key_exists('email', $data)) {
            $model->email = $data['email'];
        }

        if (array_key_exists('password', $data)) {
            $model->password = bcrypt($data['password']);
        }

        $model->save();

        if (\array_key_exists('media', $data) and !empty($data['media'])) {
            $imageData = [
                'type_id' => 'image',
                'related_to' => 'user',
                'related_id' => $model->id,
            ];

            $media = $this->mediaService->saveMedia(
                new Media(),
                $imageData,
                $data['media'],
                [
                    'width' => 360,
                    'height' => 360,
                ],
                [
                    'width' => 1600,
                    'height' => 1600,
                    'canvasWidth' => null,
                    'canvasHeight' => null,
                ]
            );
        }

        return $model;
    }
}
