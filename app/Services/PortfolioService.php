<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\Media;
use App\Models\Portfolio;
use App\Models\WorkCategory;
use App\Services\MediaService;

class PortfolioService
{
    /**
     * @var MediaService
     */
    protected $mediaService;

    public function __construct(MediaService $media)
    {
        $this->mediaService = $media;
    }

    public function getAdminList(int $pageLength = 30): LengthAwarePaginator
    {
        return Portfolio::orderBy('featured_at', 'desc')
            ->orderBy('published_at', 'desc')
            ->orderBy('created_at', 'desc')
            ->paginate();
    }

    public function adminAllExcept(Portfolio $portfolio): Collection
    {
        return Portfolio::orderBy('created_at', 'desc')
            ->with([
                'workCategories',
            ])
            ->where('id', '!=', $portfolio->id)
            ->get();
    }

    public function getList(): Collection
    {
        return Portfolio::with([
                'media',
                'workCategories',
                'users',
                'thumbnail',
            ])
            ->where('is_visible', true)
            ->orderBy('published_at', 'desc')
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function getListFeatured(): Collection
    {
        return Portfolio::with([
                'media',
                'workCategories',
                'users',
                'thumbnail',
            ])
            ->whereNotNull('featured_at')
            ->where('is_visible', true)
            ->orderBy('featured_at', 'desc')
            ->get();
    }

    public function getBySlug(
        string $slug,
        array $notIn = [],
        string $lang = null
    ): ?Portfolio {
        $query = Portfolio::query();

        if (!empty($notIn)) {
            $query->whereNotIn('id', $notIn);
        }

        if (!empty($lang)) {
            $query->whereRaw("json_value(slug, '$.$lang') = '" . $slug . "'");
        } else {
            $query->where(function ($query) use ($slug) {
                foreach (config('ba.available_languages') as $lang) {
                    $query->orWhereRaw("json_value(slug, '$.$lang') = '" . $slug . "'");
                }
            });
        }

        return $query->first();
    }

    public function save(
        Portfolio $model,
        string $lang,
        array $data
    ): Portfolio {
        DB::beginTransaction();

        if (array_key_exists('title', $data)) {
            $model->setTranslation('title', $lang, $data['title']);

            $slugOriginal = str_slug($data['title']);
            $slug = $slugOriginal;
            $i = 1;

            while (!empty($this->getBySlug($slug, [$model->id], $lang))) {
                $slug = $slugOriginal . '-' . $i;
                $i++;
            }

            $model->slug = $slug;
        }

        if (array_key_exists('body', $data)) {
            $model->setTranslation('body', $lang, $data['body']);
        }

        if (array_key_exists('customer_name', $data)) {
            $model->setTranslation('customer_name', $lang, $data['customer_name']);
        }

        if (array_key_exists('description', $data)) {
            $model->setTranslation('description', $lang, $data['description']);
        }

        if (array_key_exists('published_at', $data)) {
            $model->published_at = $data['published_at'];
        }

        if (array_key_exists('published_at_format', $data)) {
            $model->published_at_format = $data['published_at_format'];
        }

        if (array_key_exists('featured_at', $data)) {
            $model->featured_at = $data['featured_at'];
        }

        if (array_key_exists('link', $data)) {
            $model->link = $data['link'];
        }

        if (array_key_exists('view_type_id', $data)) {
            $model->view_type_id = $data['view_type_id'];
        }

        if (array_key_exists('is_visible', $data)) {
            $model->is_visible = (boolean)$data['is_visible'];
        }

        $model->save();

        if (array_key_exists('category_ids', $data)) {
            $model->workCategories()->sync($data['category_ids']);
        }

        if (array_key_exists('user_ids', $data)) {
            $model->users()->sync($data['user_ids']);
        }

        if (array_key_exists('related_portfolio_ids', $data)) {
            $model->relatedPortfolios()->sync($data['related_portfolio_ids']);
        }

        if (\array_key_exists('media', $data) and !empty($data['media'])) {
            $imageData = [
                'type_id' => 'image',
                'related_to' => 'portfolio',
                'related_id' => $model->id,
            ];

            $mediaIds = [];

            foreach ($data['media'] as $item) {
                $media = $this->mediaService->saveMedia(
                    new Media(),
                    $imageData,
                    $item,
                    [
                        'width' => 600,
                        'height' => 600,
                    ],
                    false
                );

                $mediaIds[] = $media->id;
            }
        }

        if (\array_key_exists('thumbnail', $data) and !empty($data['thumbnail'])) {
            if ($model->thumbnail) {
                $model->thumbnail->delete();
            }

            $imageData = [
                'type_id' => 'image',
                'related_to' => 'portfolio-thumbnail',
                'related_id' => $model->id,
            ];

            $media = $this->mediaService->saveMedia(
                new Media(),
                $imageData,
                $data['thumbnail'],
                [
                    'width' => 300,
                    'height' => 300,
                ],
                false
            );
        }

        if (\array_key_exists('video', $data) and !empty($data['video'])) {
            if ($model->video) {
                $model->video->delete();
            }

            $imageData = [
                'type_id' => 'video',
                'related_to' => 'portfolio-video',
                'related_id' => $model->id,
            ];

            $media = $this->mediaService->saveMedia(
                new Media(),
                $imageData,
                $data['video']
            );
        }

        DB::commit();

        return $model;
    }

    public function getCategoryAdminList(): Collection
    {
        return WorkCategory::all();
    }

    public function getCategoryList(): Collection
    {
        return WorkCategory::orderBy('title')
            ->with([
                'icon',
            ])
            ->get();
    }

    public function getActiveCategoryList(): Collection
    {
        return WorkCategory::orderBy('title')
            ->join('porfolio_categories AS pc', 'pc.work_category_id', '=', 'work_categories.id')
            ->join('portfolios AS p', 'p.id', '=', 'pc.portfolio_id')
            ->select([
                'work_categories.*',
            ])
            ->whereNull('p.deleted_at')
            ->groupBy('work_categories.id')
            ->groupBy('work_categories.title')
            ->groupBy('work_categories.body')
            ->groupBy('work_categories.slug')
            ->groupBy('work_categories.created_at')
            ->groupBy('work_categories.updated_at')
            ->with([
                'icon',
            ])
            ->get();
    }

    public function getCategoryBySlug(
        string $slug,
        array $notIn = [],
        string $lang = null
    ): ?WorkCategory {
        $query = WorkCategory::query();

        if (!empty($notIn)) {
            $query->whereNotIn('id', $notIn);
        }

        if (!empty($lang)) {
            $query->whereRaw("json_value(title, '$.$lang') = '" . $slug . "'");
        } else {
            $query->where(function ($query) use ($slug) {
                foreach (config('ba.available_languages') as $lang) {
                    $query->orWhereRaw("json_value(title, '$.$lang') = '" . $slug . "'");
                }
            });
        }

        return $query->first();
    }

    public function saveCategory(
        WorkCategory $model,
        string $lang,
        array $data
    ): WorkCategory {
        DB::beginTransaction();

        if (array_key_exists('title', $data)) {
            $model->setTranslation('title', $lang, $data['title']);

            $slugOriginal = str_slug($data['title']);
            $slug = $slugOriginal;
            $i = 1;

            while (!empty($this->getCategoryBySlug($slug, [$model->id], $lang))) {
                $slug = $slugOriginal . '-' . $i;
                $i++;
            }

            $model->slug = $slug;
        }

        if (array_key_exists('body', $data)) {
            $model->setTranslation('body', $lang, $data['body']);
        }

        $model->save();

        if (\array_key_exists('icon', $data) and !empty($data['icon'])) {
            if ($model->icon) {
                $model->icon->delete();
            }

            $imageData = [
                'type_id' => 'image',
                'related_to' => 'work-category-icon',
                'related_id' => $model->id,
            ];

            // dd($data['icon']);

            $media = $this->mediaService->saveMedia(
                new Media(),
                $imageData,
                $data['icon']
            );
        }

        DB::commit();

        return $model;
    }
}
