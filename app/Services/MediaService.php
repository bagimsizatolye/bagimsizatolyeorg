<?php

namespace App\Services;

use DB;
use Illuminate\Http\UploadedFile;

use Intervention\Image\Facades\Image;

use App\Models\Media;

class MediaService
{
    public function getMedia(int $id): ?Media
    {
        return Media::find($id);
    }

    /**
     * Saves the media to file system and database
     * @param  Media   $media            Media model
     * @param  array   $post             Data to save in database
     * @param  \Illuminate\Http\UploadedFile $file Uploaded file if available
     * @param  boolean|array $generateThumb An array with width and height keys
     * @param  boolean $resizeImageSizes An array with width, height,
     * canvasWidth, canvasHeight keys
     * @return \App\Models\Media
     */
    public function saveMedia(
        Media $media,
        array $post,
        UploadedFile $file = null,
        $generateThumb = false,
        $resizeImageSizes = false
    ): Media {
        if (!empty($post['type_id'])) {
            $media->type_id = $post['type_id'];
        }

        if (!empty($post['related_id'])) {
            $media->related_id = $post['related_id'];
        }

        if (!empty($post['related_to'])) {
            $media->related_to = $post['related_to'];
        }

        if (!empty($post['title'])) {
            $media->title = $post['title'];
        }

        if ($media->related_id) {
            $this->increaseMediaOrderNums($media->related_to, $media->related_id);
        }

        if ('video' === $media->type_id and !empty($file)) {
            $path = $file->storeAs('public/videos', md5(microtime()) . '.mp4');
            $media->path = $path;
        } elseif (
            'image' === $media->type_id and
            null !== $file and
            'image/svg' === $file->getMimeType()
        ) {
            $path = $file->storeAs('public/images', md5(microtime()) . '.svg');
            $media->path = $path;
        } elseif ('image' === $media->type_id and null !== $file) {
            $path = $file->store('public/images');
            $image = Image::make(storage_path("app/$path"));

            if (false !== $resizeImageSizes) {
                $image = Image::make(storage_path("app/$path"))
                    ->orientate();

                if (isset($resizeImageSizes['crop_x'])) {
                    $image->crop(
                        $resizeImageSizes['crop_width'],
                        $resizeImageSizes['crop_height'],
                        $resizeImageSizes['crop_x'],
                        $resizeImageSizes['crop_y']
                    );
                }

                $image = $image->resize(
                        $resizeImageSizes['width'],
                        $resizeImageSizes['height'],
                        function ($constraint) {
                            $constraint->aspectRatio();
                        }
                    )->resizeCanvas(
                        $resizeImageSizes['canvasWidth'],
                        $resizeImageSizes['canvasHeight']
                    )->save(storage_path("app/$path"), 70);
            }

            $media->width = $image->width();
            $media->height = $image->height();
            $media->path = $path;
        }

        if ('image' === $media->type_id and $generateThumb) {
            $pathThumb = $this->getThumbnailPath($path);
            $pathThumbToSave = $this->getThumbnailPath(storage_path("app/$path"));
            $imageThumb = Image::make(storage_path("app/$path"))
                ->orientate()
                ->resize(
                    null,
                    $generateThumb['height'],
                    function ($constraint) {
                        $constraint->aspectRatio();
                    }
                );

            if (isset($generateThumb['width'])) {
                $imageThumb->resizeCanvas(
                    $generateThumb['width'],
                    $generateThumb['height']
                );
            }

            $imageThumb->save($pathThumbToSave, 70);

            $media->path_thumbnail = $pathThumb;
        }

        $media->save();

        return $media;
    }

    public function deleteMedia(int $id)
    {
        $model = Media::find($id);

        // DO NOT DELETE THE FILES
        // Search engines index them
        //
        // if($model->type_id === 'image') {
        //     if(!empty($model->path)) {
        //         $path = storage_path('app/' . $model->path);
        //         if(file_exists($path)) {
        //             unlink($path);
        //         }
        //     }
        //
        //     if(!empty($model->path_thumbnail)) {
        //         $pathThumbnail = storage_path('app/' . $model->path_thumbnail);
        //         if(file_exists($path_thumbnail)) {
        //             unlink($pathThumbnail);
        //         }
        //     }
        // }

        $model->delete();

        return true;
    }

    protected function getThumbnailPath(string $path)
    {
        $path = array_reverse(explode('.', $path));

        return implode('.', [$path[1].'-t', $path[0]]);
    }

    public function correctizeOrderNum($relatedId)
    {
        $medias = Media::query();
        if ($relatedId) {
            $medias->where('related_id', $relatedId);
        }
        $medias = $medias
            ->orderBy(DB::raw('order_num, id'), 'ASC')
            ->get();

        $i = 0;
        DB::beginTransaction();
        foreach ($medias as $media) {
            $media->order_num = ++$i;
            $media->save();
        }
        DB::commit();
    }

    public function getMediaByOrderNum($order_num, $related_id, $direction)
    {
        $media = Media::where('related_id', $related_id);
        if ('up' === $direction) {
            $media->where('order_num', '<', $order_num)
                ->orderBy('order_num', 'DESC');
        } else {
            $media->where('order_num', '>', $order_num)
                ->orderBy('order_num', 'ASC');
        }

        return $media->first();
    }

    public function updateOrderNum(Media $media, $orderNum)
    {
        $media->order_num = $orderNum;
        $media->save();
    }

    private function increaseMediaOrderNums($related_to, $related_id)
    {
        Media::where('related_to', $related_to)
            ->where('related_id', $related_id)
            ->orderBy('order_num', 'desc')
            ->increment('order_num');
    }

    public function orderMedia(Media $media, int $newIndex, int $oldIndex): Media
    {
        DB::beginTransaction();

        if ($newIndex > $oldIndex) {
            $media->order_num = rand(-99999, -9999);
            $media->save();

            $limitOrderNumber = Media::where('related_id', $media->related_id)
                ->where('related_to', $media->related_to)
                ->orderBy('order_num', 'asc')
                ->offset($newIndex)
                ->first()
                ->order_num;

            Media::where('related_id', $media->related_id)
                ->where('related_to', $media->related_to)
                ->where('order_num', '<=', $limitOrderNumber)
                ->orderBy('order_num', 'asc')
                ->decrement('order_num');
        } else {
            $media->order_num = rand(9999, 99999);
            $media->save();

            $limitOrderNumber = Media::where('related_id', $media->related_id)
                ->where('related_to', $media->related_to)
                ->orderBy('order_num', 'asc')
                ->offset($newIndex)
                ->first()
                ->order_num;

            Media::where('related_id', $media->related_id)
                ->where('related_to', $media->related_to)
                ->where('order_num', '>=', $limitOrderNumber)
                ->orderBy('order_num', 'desc')
                ->increment('order_num');
        }

        $media->order_num = $limitOrderNumber;
        $media->save();

        DB::commit();

        return $media;
    }
}
