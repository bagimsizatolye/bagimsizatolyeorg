<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\Blog;
use App\Models\Media;
use App\Models\Tag;
use App\Services\MediaService;

class BlogService
{
    /**
     * @var MediaService
     */
    protected $mediaService;

    public function __construct(
        MediaService $mediaService
    ) {
        $this->mediaService = $mediaService;
    }

    public function getLiveList(int $limit = null, array $notIn = []): Collection
    {
        $query = Blog::whereNotNull('published_at')
            ->with([
                'cover',
                'author',
            ])
            ->orderBy('published_at', 'desc');

        if (!is_null($limit)) {
            $query->limit($limit);
        }

        if (!empty($notIn)) {
            $query->whereNotIn('id', $notIn);
        }

        return $query->get();
    }

    public function getLiveListForTag(Tag $tag): Collection
    {
        return Blog::whereNotNull('published_at')
            ->with([
                'cover',
                'author',
            ])
            ->orderBy('published_at', 'desc')
            ->join('blog_tags AS bt', 'bt.blog_id', '=', 'blogs.id')
            ->where('bt.tag_id', $tag->id)
            ->get();
    }

    public function getAdminList(): Collection
    {
        return Blog::orderBy('updated_at', 'desc')
            ->get();
    }

    public function getBySlug(string $slug, array $notIn = []): ?Blog
    {
        return Blog::where('slug', $slug)
            ->whereNotIn('id', $notIn)
            ->first();
    }

    public function loadDetailRelations(Blog &$blog): Blog
    {
        $blog->load([
            'cover',
            'author',
            'tags',
        ]);

        return $blog;
    }

    public function save(Blog $model, array $data): Blog
    {
        DB::beginTransaction();

        if (\array_key_exists('title', $data)) {
            $model->title = $data['title'];

            $slugOriginal = str_slug($data['title']);
            $slug = $slugOriginal;
            $i = 1;

            while (!empty($this->getBySlug($slug, [$model->id]))) {
                $slug = $slugOriginal . '-' . $i;
                $i++;
            }

            $model->slug = $slug;
        }

        if (\array_key_exists('body', $data)) {
            $model->body = $data['body'];
        }

        if (\array_key_exists('author_id', $data)) {
            $model->author_id = $data['author_id'];
        }

        if (\array_key_exists('published_at', $data)) {
            $model->published_at = $data['published_at'];
        }

        if (\array_key_exists('featured_at', $data)) {
            $model->featured_at = $data['featured_at'];
        }

        $model->save();

        if (\array_key_exists('tag_ids', $data)) {
            $model->tags()->sync($data['tag_ids']);
        }

        if (\array_key_exists('cover', $data) and !empty($data['cover'])) {
            $imageData = [
                'type_id' => 'image',
                'related_to' => 'blog-cover',
                'related_id' => $model->id,
            ];

            $media = $this->mediaService->saveMedia(
                new Media(),
                $imageData,
                $data['cover'],
                [
                    'width' => 800,
                    'height' => 450,
                ],
                [
                    'width' => 1600,
                    'height' => null,
                    'canvasWidth' => null,
                    'canvasHeight' => null,
                ]
            );
        }

        DB::commit();

        return $model;
    }
}
