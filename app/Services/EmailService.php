<?php

namespace App\Services;

use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class EmailService
{
    public function send(Mailable $mailable, string $to)
    {
        return Mail::to($to)->send($mailable);
    }
}
