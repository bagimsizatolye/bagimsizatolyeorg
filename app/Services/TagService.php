<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

use App\Models\Tag;

class TagService
{
    public function getAdminList(): Collection
    {
        return Tag::orderBy('title')
            ->limit(1000)
            ->get();
    }

    public function getLiveList(int $limit = null): Collection
    {
        $query = Tag::orderBy('tags.title')
            ->join('blog_tags AS bt', 'bt.tag_id', '=', 'tags.id')
            ->join('blogs AS b', 'b.id', '=', 'bt.blog_id')
            ->whereNull('b.deleted_at')
            ->groupBy('tags.id')
            ->groupBy('tags.title')
            ->groupBy('tags.slug')
            ->select('tags.*');

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query->get();
    }

    public function getBySlug(string $slug, array $notIn = []): ?Tag
    {
        return Tag::where('slug', $slug)
            ->whereNotIn('id', $notIn)
            ->first();
    }

    public function save(Tag $model, array $data): Tag
    {
        DB::beginTransaction();

        if (\array_key_exists('title', $data)) {
            $model->title = $data['title'];

            $slugOriginal = str_slug($data['title']);
            $slug = $slugOriginal;
            $i = 1;

            while (!empty($this->getBySlug($slug, [$model->id]))) {
                $slug = $slugOriginal . '-' . $i;
                $i++;
            }

            $model->slug = $slug;
        }

        $model->save();

        DB::commit();

        return $model;
    }
}
