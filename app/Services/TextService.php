<?php

namespace App\Services;

use App\Models\Text;

class TextService
{
    /**
     * @var array
     */
    protected $valuesList = [];

    public function getAdminList(): array
    {
        $configList = config('ba.texts');
        $langList = config('ba.available_languages');

        if (empty($this->valuesList)) {
            $this->valuesList = Text::query()->get();
        }

        $valuesList = $this->valuesList;
        $textList = [];

        foreach ($configList as $key) {
            foreach ($langList as $lang) {
                if (!isset($textList[$key])) {
                    $textList[$key] = [];
                }

                if (!isset($textList[$key][$lang])) {
                    $textList[$key][$lang] = [];
                }

                $index = $valuesList->search(
                    function ($item) use ($key, $lang) {
                        return $item->key === $key
                            and $item->lang === $lang;
                    }
                );

                if ($index !== false) {
                    $textList[$key][$lang] = $valuesList[$index]->value;
                } else {
                    $textList[$key][$lang] = null;
                }
            }
        }

        return $textList;
    }

    public function getText(string $key, string $lang): ?Text
    {
        return Text::where('key', $key)
            ->where('lang', $lang)
            ->first();
    }

    public function saveText(
        Text $text,
        string $key,
        string $lang,
        string $value = null
    ): Text {
        $text->key = $key;
        $text->lang = $lang;
        $text->value = $value;
        $text->save();

        return $text;
    }

    protected function setValuesList()
    {
        $this->valuesList = $this->getAdminList();
    }

    public function getTextString(string $key, string $lang): ?string
    {
        if (empty($this->valuesList)) {
            $this->setValuesList();
        }

        if (isset($this->valuesList[$key][$lang])) {
            return $this->valuesList[$key][$lang] ?: null;
        }

        return null;
    }
}
