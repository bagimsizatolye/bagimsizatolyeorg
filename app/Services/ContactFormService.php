<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Mail\NewContactForm;
use App\Models\ContactForm;
use App\Models\User;
use App\Services\EmailService;

class ContactFormService
{
    /**
     * @var EmailService
     */
    protected $emailService;

    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    public function getAdminList(int $pageLength = 30): LengthAwarePaginator
    {
        return ContactForm::orderBy(DB::raw('answered_at IS NOT NULL'), 'asc')
            ->orderBy('created_at')
            ->with([
                'answeredBy',
            ])
            ->paginate();
    }

    public function save(ContactForm $model, array $formData): ContactForm
    {
        if (array_key_exists('answered_at', $formData)) {
            $model->answered_at = $formData['answered_at'];
        }

        if (array_key_exists('answered_by_id', $formData)) {
            $model->answered_by_id = $formData['answered_by_id'];
        }

        if (array_key_exists('name', $formData)) {
            $model->name = $formData['name'];
        }

        if (array_key_exists('email', $formData)) {
            $model->email = $formData['email'];
        }

        if (array_key_exists('subject', $formData)) {
            $model->subject = $formData['subject'];
        }

        if (array_key_exists('text', $formData)) {
            $model->text = $formData['text'];
        }

        $sendEmail = false;

        if (empty($model->id)) {
            $sendEmail = true;
        }

        $model->save();

        if ($sendEmail) {
            $this->emailService->send(new NewContactForm($model), config('ba.admin_email'));
        }

        return $model;
    }
}
