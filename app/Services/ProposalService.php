<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\Proposal;

class ProposalService
{
    public function getAdminList(): Collection
    {
        return Proposal::orderBy('proposed_on', 'desc')
            ->get();
    }

    public function getByCode(string $code): ?Proposal
    {
        return Proposal::where('code', $code)->first();
    }

    public function save(Proposal $model, array $data): Proposal
    {
        DB::beginTransaction();

        if (\array_key_exists('concern_company_name', $data)) {
            $model->concern_company_name = $data['concern_company_name'];
        }

        if (\array_key_exists('concern_person_name', $data)) {
            $model->concern_person_name = $data['concern_person_name'];
        }

        if (\array_key_exists('accepted_at', $data)) {
            $model->accepted_at = $data['accepted_at'];
        }

        if (\array_key_exists('rejected_at', $data)) {
            $model->rejected_at = $data['rejected_at'];
        }

        if (\array_key_exists('proposal_type', $data)) {
            $model->proposal_type = $data['proposal_type'];
        }

        if (\array_key_exists('proposed_on', $data)) {
            $model->proposed_on = $data['proposed_on'];
        }

        if (\array_key_exists('delivery_days', $data)) {
            $model->delivery_days = $data['delivery_days'];
        }

        if (\array_key_exists('bank_account', $data)) {
            $model->bank_account = $data['bank_account'];
        }

        if (!$model->id) {
            $model->code = strtolower(\str_random(8));
        }

        $model->save();

        DB::commit();

        return $model;
    }
}
