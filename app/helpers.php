<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;

use Carbon\Carbon;

use App\Services\TextService;

function format_datetime(Carbon $carbon, $format = '%e %b %Y %H:%M'): string
{
    return $carbon->locale('tr')->formatLocalized($format);
}

function format_date_fe(Carbon $carbon, $format = 'D MMMM YYYY'): string
{
    return $carbon->locale(app()->getLocale())->isoFormat($format);
}

function text(string $key, bool $sanitize = false): string
{
    /**
     * @var TextService
     */
    $service = app()->make(TextService::class);
    $text = $service->getTextString($key, app()->getLocale()) ?: '';
    if ($sanitize) {
        return strip_tags($text, '<p><br><strong><b><i><em><a>');
    }

    return $text;
}

function ba_route(
    string $routeName,
    $routeParams = [],
    string $locale = null
): string {
    if (is_null($locale)) {
        $locale = app()->getLocale();
    }

    if ($locale === config('app.fallback_locale')) {
        return route($routeName, $routeParams);
    }

    return route($locale . '.' . $routeName, $routeParams);
}

function ba_get_route_in_locale(
    Request $request,
    string $targetLocale
): string {
    $prefix = $request->route()->getPrefix();
    $routeName = $request->route()->getName();
    $params = array_merge(
        $request->input(),
        $request->route()->originalParameters()
    );

    if (is_null($prefix)) {
        return ba_route($routeName, $params, $targetLocale);
    }

    // strip the locale parameter from route name
    $prefix = str_replace('/', '', $prefix);
    $routeName = substr($routeName, 3);
    // $routeName = str_replace($prefix . '.', '', $routeName);

    return ba_route($routeName, $params, $targetLocale);
}

function set_locale(string $locale)
{
    Carbon::setLocale($locale);
    App::setLocale($locale);

    if ($locale === 'tr') {
        setlocale(LC_TIME, 'tr_TR.utf8');
    } else {
        setlocale(LC_TIME, 'en_US.utf8');
    }

    Cookie::queue('locale', $locale, 60 * 24 * 30);

    return $locale;
}
